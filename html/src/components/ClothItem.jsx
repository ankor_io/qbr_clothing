import React, { useEffect, useState } from 'react'
import Slider, { Range } from 'rc-slider';
import { Card, Grid, CardActionArea, CardMedia, CardContent, requirePropFactory } from '@mui/material'
import 'rc-slider/assets/index.css'


export default function ClothItem({ data }) {


  const groupBy = (collection, property) => {
    var i = 0, val, index,
        values = [], result = [];
    for (; i < collection.length; i++) {
        val = collection[i][property];
        index = values.indexOf(val);
        if (index > -1)
            result[index].push(collection[i]);
        else {
            values.push(val);
            result.push([collection[i]]);
        }
    }
    return result;
}

  return (
    <div className="skinItem">
      <div className='skinItem_text'>
        <p className='skinItem_text_header'>{data['name'].split('_').join(' - ') + " " + data.maxValue}</p>
        <Grid container spacing={2}>
          {
            groupBy(data.clothRef, "clothName").map(function (clothRefGroup, index) {
              console.log(clothRefGroup)
              return<Grid item key={clothRefGroup[0].id}>
                  <Card key={clothRefGroup[0].id} sx={{ maxWidth: 100 }}>
                    <CardActionArea>
                      <CardMedia
                          component="img"
                          width="100"
                          image={'/assets/' + clothRefGroup[0].clothName + '.png'}
                        />
                    </CardActionArea>
                  </Card>
              </Grid>
            })
          }
        </Grid>
      </div>
    </div>
  )
}