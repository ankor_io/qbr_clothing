const MockData = {
    skins: [
      {
        name: "hair",
        currentValue: 0,
        maxValue: 425,
        minValue: 0
      },
      {
        name: "eyes",
        currentValue: 0,
        maxValue: 14,
        minValue: 0
      },
      {
        name: "beard",
        currentValue: 0,
        maxValue: 306,
        minValue: 0
      },
      {
        name: "teeth",
        currentValue: 0,
        maxValue: 7,
        minValue: 0
      },
      {
        name: "heads",
        currentValue: 0,
        maxValue: 120,
        minValue: 0
      },
      {
        name: "skin",
        currentValue: 0,
        maxValue: 6,
        minValue: 0
      },
      {
        name: "eyelid_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nose_angle",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "ears_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyebrow_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyes_distance",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyes_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nostrils_distance",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyebrow_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyebrow_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "mouth_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "chin_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyelid_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "ears_size",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nose_curvature",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "jaw_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "lower_lip_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyes_angle",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "lower_lip_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "lower_lip_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "jaw_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nose_size",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "chin_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "cheekbones_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "jaw_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "upper_lip_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "upper_lip_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "mouth_x_pos",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "face_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "upper_lip_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "ears_angle",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nose_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "mouth_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "mouth_y_pos",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "eyes_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "cheekbones_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "nose_width",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "ears_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "chin_height",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      },
      {
        name: "cheekbones_depth",
        currentValue: 0,
        maxValue: 100,
        minValue: -100
      }
    ],
    outfits: [
      {
        model: "-171876066",
        citizenid: "YYC75958",
        outfitId: "outfit-7-3884",
        skin: [],
        outfitname: "Test",
        id: 1
      }
    ],
    clothes: [
      {
        name: "gloves",
        clothRef: [
          {
            clothName: "gloves_000",
            id: 1
          },
          {
            clothName: "gloves_000",
            id: 2
          },
          {
            clothName: "gloves_000",
            id: 3
          },
          {
            clothName: "gloves_000",
            id: 4
          },
          {
            clothName: "gloves_000",
            id: 5
          },
          {
            clothName: "gloves_000",
            id: 6
          },
          {
            clothName: "gloves_000",
            id: 7
          },
          {
            clothName: "gloves_000",
            id: 8
          },
          {
            clothName: "gloves_000",
            id: 9
          },
          {
            clothName: "gloves_000",
            id: 10
          },
          {
            clothName: "gloves_001",
            id: 11
          },
          {
            clothName: "gloves_001",
            id: 12
          },
          {
            clothName: "gloves_001",
            id: 13
          },
          {
            clothName: "gloves_001",
            id: 14
          },
          {
            clothName: "gloves_001",
            id: 15
          },
          {
            clothName: "gloves_001",
            id: 16
          },
          {
            clothName: "gloves_001",
            id: 17
          },
          {
            clothName: "gloves_001",
            id: 18
          },
          {
            clothName: "gloves_001",
            id: 19
          },
          {
            clothName: "gloves_001",
            id: 20
          },
          {
            clothName: "gloves_001",
            id: 21
          },
          {
            clothName: "gloves_001",
            id: 22
          },
          {
            clothName: "gloves_002",
            id: 23
          },
          {
            clothName: "gloves_002",
            id: 24
          },
          {
            clothName: "gloves_002",
            id: 25
          },
          {
            clothName: "gloves_002",
            id: 26
          },
          {
            clothName: "gloves_002",
            id: 27
          },
          {
            clothName: "gloves_002",
            id: 28
          },
          {
            clothName: "gloves_002",
            id: 29
          },
          {
            clothName: "gloves_002",
            id: 30
          },
          {
            clothName: "gloves_002",
            id: 31
          },
          {
            clothName: "gloves_002",
            id: 32
          },
          {
            clothName: "gloves_003",
            id: 33
          },
          {
            clothName: "gloves_003",
            id: 34
          },
          {
            clothName: "gloves_003",
            id: 35
          },
          {
            clothName: "gloves_003",
            id: 36
          },
          {
            clothName: "gloves_003",
            id: 37
          },
          {
            clothName: "gloves_003",
            id: 38
          },
          {
            clothName: "gloves_003",
            id: 39
          },
          {
            clothName: "gloves_003",
            id: 40
          },
          {
            clothName: "gloves_003",
            id: 41
          },
          {
            clothName: "gloves_003",
            id: 42
          },
          {
            clothName: "gloves_004",
            id: 43
          },
          {
            clothName: "gloves_004",
            id: 44
          },
          {
            clothName: "gloves_004",
            id: 45
          },
          {
            clothName: "gloves_004",
            id: 46
          },
          {
            clothName: "gloves_004",
            id: 47
          },
          {
            clothName: "gloves_004",
            id: 48
          },
          {
            clothName: "gloves_004",
            id: 49
          },
          {
            clothName: "gloves_004",
            id: 50
          },
          {
            clothName: "gloves_004",
            id: 51
          },
          {
            clothName: "gloves_004",
            id: 52
          },
          {
            clothName: "gloves_005",
            id: 53
          },
          {
            clothName: "gloves_005",
            id: 54
          },
          {
            clothName: "gloves_005",
            id: 55
          },
          {
            clothName: "gloves_005",
            id: 56
          },
          {
            clothName: "gloves_005",
            id: 57
          },
          {
            clothName: "gloves_005",
            id: 58
          },
          {
            clothName: "gloves_005",
            id: 59
          },
          {
            clothName: "gloves_005",
            id: 60
          },
          {
            clothName: "gloves_005",
            id: 61
          },
          {
            clothName: "gloves_005",
            id: 62
          },
          {
            clothName: "gloves_006",
            id: 63
          },
          {
            clothName: "gloves_006",
            id: 64
          },
          {
            clothName: "gloves_006",
            id: 65
          },
          {
            clothName: "gloves_006",
            id: 66
          },
          {
            clothName: "gloves_006",
            id: 67
          },
          {
            clothName: "gloves_006",
            id: 68
          },
          {
            clothName: "gloves_006",
            id: 69
          },
          {
            clothName: "gloves_006",
            id: 70
          },
          {
            clothName: "gloves_006",
            id: 71
          },
          {
            clothName: "gloves_006",
            id: 72
          },
          {
            clothName: "gloves_007",
            id: 73
          },
          {
            clothName: "gloves_007",
            id: 74
          },
          {
            clothName: "gloves_007",
            id: 75
          },
          {
            clothName: "gloves_007",
            id: 76
          },
          {
            clothName: "gloves_007",
            id: 77
          },
          {
            clothName: "gloves_007",
            id: 78
          },
          {
            clothName: "gloves_007",
            id: 79
          },
          {
            clothName: "gloves_007",
            id: 80
          },
          {
            clothName: "gloves_007",
            id: 81
          },
          {
            clothName: "gloves_007",
            id: 82
          },
          {
            clothName: "m_gloves_008",
            id: 83
          },
          {
            clothName: "m_gloves_008",
            id: 84
          },
          {
            clothName: "m_gloves_009",
            id: 85
          },
          {
            clothName: "m_gloves_009",
            id: 86
          },
          {
            clothName: "m_gloves_009",
            id: 87
          },
          {
            clothName: "m_gloves_009",
            id: 88
          },
          {
            clothName: "m_gloves_009",
            id: 89
          },
          {
            clothName: "m_gloves_009",
            id: 90
          },
          {
            clothName: "m_gloves_010",
            id: 91
          },
          {
            clothName: "m_gloves_010",
            id: 92
          },
          {
            clothName: "m_gloves_011",
            id: 93
          },
          {
            clothName: "m_gloves_011",
            id: 94
          },
          {
            clothName: "m_gloves_011",
            id: 95
          },
          {
            clothName: "m_gloves_011",
            id: 96
          },
          {
            clothName: "m_gloves_011",
            id: 97
          },
          {
            clothName: "m_gloves_011",
            id: 98
          },
          {
            clothName: "m_gloves_011",
            id: 99
          },
          {
            clothName: "m_gloves_011",
            id: 100
          },
          {
            clothName: "m_gloves_200",
            id: 101
          },
          {
            clothName: "m_gloves_200",
            id: 102
          },
          {
            clothName: "m_gloves_200",
            id: 103
          },
          {
            clothName: "m_gloves_200",
            id: 104
          },
          {
            clothName: "m_gloves_200",
            id: 105
          },
          {
            clothName: "m_gloves_200",
            id: 106
          },
          {
            clothName: "m_gloves_200",
            id: 107
          },
          {
            clothName: "m_gloves_200",
            id: 108
          },
          {
            clothName: "m_gloves_200",
            id: 109
          },
          {
            clothName: "m_gloves_200",
            id: 110
          },
          {
            clothName: "unknown_item",
            id: 111
          },
          {
            clothName: "unknown_item",
            id: 112
          },
          {
            clothName: "unknown_item",
            id: 113
          },
          {
            clothName: "unknown_item",
            id: 114
          },
          {
            clothName: "unknown_item",
            id: 115
          },
          {
            clothName: "unknown_item",
            id: 116
          },
          {
            clothName: "gloves_000",
            id: 117
          },
          {
            clothName: "gloves_000",
            id: 118
          },
          {
            clothName: "unknown_item",
            id: 119
          }
        ],
        maxValue: 119,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "belt_buckles",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "unknown_item",
            id: 2
          },
          {
            clothName: "unknown_item",
            id: 3
          },
          {
            clothName: "unknown_item",
            id: 4
          },
          {
            clothName: "unknown_item",
            id: 5
          },
          {
            clothName: "unknown_item",
            id: 6
          },
          {
            clothName: "unknown_item",
            id: 7
          },
          {
            clothName: "unknown_item",
            id: 8
          },
          {
            clothName: "unknown_item",
            id: 9
          },
          {
            clothName: "unknown_item",
            id: 10
          },
          {
            clothName: "unknown_item",
            id: 11
          },
          {
            clothName: "unknown_item",
            id: 12
          },
          {
            clothName: "unknown_item",
            id: 13
          },
          {
            clothName: "unknown_item",
            id: 14
          },
          {
            clothName: "unknown_item",
            id: 15
          },
          {
            clothName: "unknown_item",
            id: 16
          },
          {
            clothName: "unknown_item",
            id: 17
          },
          {
            clothName: "unknown_item",
            id: 18
          },
          {
            clothName: "unknown_item",
            id: 19
          },
          {
            clothName: "unknown_item",
            id: 20
          },
          {
            clothName: "unknown_item",
            id: 21
          },
          {
            clothName: "unknown_item",
            id: 22
          },
          {
            clothName: "unknown_item",
            id: 23
          },
          {
            clothName: "unknown_item",
            id: 24
          },
          {
            clothName: "unknown_item",
            id: 25
          },
          {
            clothName: "unknown_item",
            id: 26
          },
          {
            clothName: "unknown_item",
            id: 27
          },
          {
            clothName: "unknown_item",
            id: 28
          },
          {
            clothName: "unknown_item",
            id: 29
          },
          {
            clothName: "unknown_item",
            id: 30
          },
          {
            clothName: "unknown_item",
            id: 31
          },
          {
            clothName: "unknown_item",
            id: 32
          },
          {
            clothName: "unknown_item",
            id: 33
          },
          {
            clothName: "unknown_item",
            id: 34
          },
          {
            clothName: "unknown_item",
            id: 35
          },
          {
            clothName: "unknown_item",
            id: 36
          },
          {
            clothName: "unknown_item",
            id: 37
          },
          {
            clothName: "unknown_item",
            id: 38
          },
          {
            clothName: "unknown_item",
            id: 39
          },
          {
            clothName: "unknown_item",
            id: 40
          },
          {
            clothName: "unknown_item",
            id: 41
          },
          {
            clothName: "unknown_item",
            id: 42
          },
          {
            clothName: "unknown_item",
            id: 43
          },
          {
            clothName: "unknown_item",
            id: 44
          },
          {
            clothName: "unknown_item",
            id: 45
          },
          {
            clothName: "unknown_item",
            id: 46
          },
          {
            clothName: "unknown_item",
            id: 47
          },
          {
            clothName: "unknown_item",
            id: 48
          },
          {
            clothName: "unknown_item",
            id: 49
          },
          {
            clothName: "unknown_item",
            id: 50
          },
          {
            clothName: "unknown_item",
            id: 51
          },
          {
            clothName: "unknown_item",
            id: 52
          },
          {
            clothName: "unknown_item",
            id: 53
          },
          {
            clothName: "unknown_item",
            id: 54
          },
          {
            clothName: "unknown_item",
            id: 55
          },
          {
            clothName: "unknown_item",
            id: 56
          },
          {
            clothName: "unknown_item",
            id: 57
          },
          {
            clothName: "unknown_item",
            id: 58
          },
          {
            clothName: "unknown_item",
            id: 59
          },
          {
            clothName: "unknown_item",
            id: 60
          },
          {
            clothName: "unknown_item",
            id: 61
          },
          {
            clothName: "unknown_item",
            id: 62
          },
          {
            clothName: "unknown_item",
            id: 63
          },
          {
            clothName: "unknown_item",
            id: 64
          },
          {
            clothName: "unknown_item",
            id: 65
          },
          {
            clothName: "unknown_item",
            id: 66
          },
          {
            clothName: "unknown_item",
            id: 67
          },
          {
            clothName: "unknown_item",
            id: 68
          },
          {
            clothName: "unknown_item",
            id: 69
          },
          {
            clothName: "unknown_item",
            id: 70
          },
          {
            clothName: "unknown_item",
            id: 71
          },
          {
            clothName: "unknown_item",
            id: 72
          },
          {
            clothName: "unknown_item",
            id: 73
          },
          {
            clothName: "unknown_item",
            id: 74
          },
          {
            clothName: "unknown_item",
            id: 75
          },
          {
            clothName: "unknown_item",
            id: 76
          },
          {
            clothName: "unknown_item",
            id: 77
          },
          {
            clothName: "unknown_item",
            id: 78
          },
          {
            clothName: "unknown_item",
            id: 79
          },
          {
            clothName: "unknown_item",
            id: 80
          },
          {
            clothName: "unknown_item",
            id: 81
          },
          {
            clothName: "unknown_item",
            id: 82
          },
          {
            clothName: "unknown_item",
            id: 83
          },
          {
            clothName: "unknown_item",
            id: 84
          },
          {
            clothName: "unknown_item",
            id: 85
          },
          {
            clothName: "unknown_item",
            id: 86
          },
          {
            clothName: "unknown_item",
            id: 87
          },
          {
            clothName: "unknown_item",
            id: 88
          },
          {
            clothName: "unknown_item",
            id: 89
          },
          {
            clothName: "unknown_item",
            id: 90
          },
          {
            clothName: "unknown_item",
            id: 91
          },
          {
            clothName: "unknown_item",
            id: 92
          },
          {
            clothName: "unknown_item",
            id: 93
          },
          {
            clothName: "unknown_item",
            id: 94
          },
          {
            clothName: "unknown_item",
            id: 95
          },
          {
            clothName: "unknown_item",
            id: 96
          },
          {
            clothName: "unknown_item",
            id: 97
          },
          {
            clothName: "unknown_item",
            id: 98
          },
          {
            clothName: "unknown_item",
            id: 99
          },
          {
            clothName: "unknown_item",
            id: 100
          },
          {
            clothName: "unknown_item",
            id: 101
          },
          {
            clothName: "unknown_item",
            id: 102
          },
          {
            clothName: "unknown_item",
            id: 103
          },
          {
            clothName: "unknown_item",
            id: 104
          },
          {
            clothName: "unknown_item",
            id: 105
          },
          {
            clothName: "unknown_item",
            id: 106
          },
          {
            clothName: "unknown_item",
            id: 107
          },
          {
            clothName: "unknown_item",
            id: 108
          },
          {
            clothName: "unknown_item",
            id: 109
          },
          {
            clothName: "unknown_item",
            id: 110
          },
          {
            clothName: "unknown_item",
            id: 111
          },
          {
            clothName: "unknown_item",
            id: 112
          },
          {
            clothName: "unknown_item",
            id: 113
          },
          {
            clothName: "unknown_item",
            id: 114
          },
          {
            clothName: "unknown_item",
            id: 115
          },
          {
            clothName: "unknown_item",
            id: 116
          },
          {
            clothName: "unknown_item",
            id: 117
          },
          {
            clothName: "unknown_item",
            id: 118
          },
          {
            clothName: "unknown_item",
            id: 119
          },
          {
            clothName: "unknown_item",
            id: 120
          },
          {
            clothName: "unknown_item",
            id: 121
          },
          {
            clothName: "unknown_item",
            id: 122
          },
          {
            clothName: "unknown_item",
            id: 123
          },
          {
            clothName: "unknown_item",
            id: 124
          },
          {
            clothName: "unknown_item",
            id: 125
          },
          {
            clothName: "unknown_item",
            id: 126
          },
          {
            clothName: "unknown_item",
            id: 127
          },
          {
            clothName: "unknown_item",
            id: 128
          },
          {
            clothName: "unknown_item",
            id: 129
          },
          {
            clothName: "unknown_item",
            id: 130
          }
        ],
        maxValue: 130,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "boot_accessories",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "unknown_item",
            id: 2
          },
          {
            clothName: "spurs_000",
            id: 3
          },
          {
            clothName: "spurs_000",
            id: 4
          },
          {
            clothName: "spurs_000",
            id: 5
          },
          {
            clothName: "spurs_000",
            id: 6
          },
          {
            clothName: "spurs_000",
            id: 7
          },
          {
            clothName: "spurs_000",
            id: 8
          },
          {
            clothName: "spurs_000",
            id: 9
          },
          {
            clothName: "spurs_000",
            id: 10
          },
          {
            clothName: "spurs_000",
            id: 11
          },
          {
            clothName: "spurs_000",
            id: 12
          },
          {
            clothName: "spurs_000",
            id: 13
          },
          {
            clothName: "spurs_001",
            id: 14
          },
          {
            clothName: "spurs_001",
            id: 15
          },
          {
            clothName: "spurs_001",
            id: 16
          },
          {
            clothName: "spurs_001",
            id: 17
          },
          {
            clothName: "spurs_001",
            id: 18
          },
          {
            clothName: "spurs_001",
            id: 19
          },
          {
            clothName: "spurs_001",
            id: 20
          },
          {
            clothName: "spurs_001",
            id: 21
          },
          {
            clothName: "spurs_001",
            id: 22
          },
          {
            clothName: "spurs_001",
            id: 23
          },
          {
            clothName: "spurs_002",
            id: 24
          },
          {
            clothName: "spurs_002",
            id: 25
          },
          {
            clothName: "spurs_002",
            id: 26
          },
          {
            clothName: "spurs_002",
            id: 27
          },
          {
            clothName: "spurs_002",
            id: 28
          },
          {
            clothName: "spurs_002",
            id: 29
          },
          {
            clothName: "spurs_002",
            id: 30
          },
          {
            clothName: "spurs_002",
            id: 31
          },
          {
            clothName: "spurs_002",
            id: 32
          },
          {
            clothName: "spurs_002",
            id: 33
          },
          {
            clothName: "spurs_003",
            id: 34
          },
          {
            clothName: "spurs_003",
            id: 35
          },
          {
            clothName: "spurs_003",
            id: 36
          },
          {
            clothName: "spurs_003",
            id: 37
          },
          {
            clothName: "spurs_003",
            id: 38
          },
          {
            clothName: "spurs_003",
            id: 39
          },
          {
            clothName: "spurs_003",
            id: 40
          },
          {
            clothName: "spurs_003",
            id: 41
          },
          {
            clothName: "spurs_003",
            id: 42
          },
          {
            clothName: "spurs_003",
            id: 43
          },
          {
            clothName: "spurs_004",
            id: 44
          },
          {
            clothName: "spurs_004",
            id: 45
          },
          {
            clothName: "spurs_004",
            id: 46
          },
          {
            clothName: "spurs_004",
            id: 47
          },
          {
            clothName: "spurs_004",
            id: 48
          },
          {
            clothName: "spurs_004",
            id: 49
          },
          {
            clothName: "spurs_004",
            id: 50
          },
          {
            clothName: "spurs_004",
            id: 51
          },
          {
            clothName: "spurs_004",
            id: 52
          },
          {
            clothName: "spurs_004",
            id: 53
          },
          {
            clothName: "spurs_007",
            id: 54
          },
          {
            clothName: "spurs_007",
            id: 55
          },
          {
            clothName: "spurs_007",
            id: 56
          },
          {
            clothName: "spurs_007",
            id: 57
          },
          {
            clothName: "spurs_007",
            id: 58
          },
          {
            clothName: "spurs_007",
            id: 59
          },
          {
            clothName: "spurs_007",
            id: 60
          },
          {
            clothName: "spurs_007",
            id: 61
          },
          {
            clothName: "spurs_007",
            id: 62
          },
          {
            clothName: "spurs_007",
            id: 63
          },
          {
            clothName: "spurs_008",
            id: 64
          },
          {
            clothName: "spurs_008",
            id: 65
          },
          {
            clothName: "spurs_008",
            id: 66
          },
          {
            clothName: "spurs_008",
            id: 67
          },
          {
            clothName: "spurs_008",
            id: 68
          },
          {
            clothName: "spurs_008",
            id: 69
          },
          {
            clothName: "spurs_008",
            id: 70
          },
          {
            clothName: "spurs_008",
            id: 71
          },
          {
            clothName: "spurs_008",
            id: 72
          },
          {
            clothName: "spurs_008",
            id: 73
          },
          {
            clothName: "spurs_009",
            id: 74
          },
          {
            clothName: "spurs_009",
            id: 75
          },
          {
            clothName: "spurs_009",
            id: 76
          },
          {
            clothName: "spurs_009",
            id: 77
          },
          {
            clothName: "spurs_009",
            id: 78
          },
          {
            clothName: "spurs_009",
            id: 79
          },
          {
            clothName: "spurs_009",
            id: 80
          },
          {
            clothName: "spurs_009",
            id: 81
          },
          {
            clothName: "spurs_009",
            id: 82
          },
          {
            clothName: "spurs_009",
            id: 83
          },
          {
            clothName: "spurs_010",
            id: 84
          },
          {
            clothName: "spurs_010",
            id: 85
          },
          {
            clothName: "spurs_010",
            id: 86
          },
          {
            clothName: "spurs_010",
            id: 87
          },
          {
            clothName: "spurs_010",
            id: 88
          },
          {
            clothName: "spurs_010",
            id: 89
          },
          {
            clothName: "spurs_010",
            id: 90
          },
          {
            clothName: "spurs_010",
            id: 91
          },
          {
            clothName: "spurs_010",
            id: 92
          },
          {
            clothName: "spurs_010",
            id: 93
          },
          {
            clothName: "m_spurs_011",
            id: 94
          },
          {
            clothName: "m_spurs_011",
            id: 95
          },
          {
            clothName: "m_spurs_011",
            id: 96
          },
          {
            clothName: "m_spurs_011",
            id: 97
          },
          {
            clothName: "m_spurs_011",
            id: 98
          },
          {
            clothName: "m_spurs_011",
            id: 99
          },
          {
            clothName: "m_spurs_011",
            id: 100
          },
          {
            clothName: "m_spurs_011",
            id: 101
          },
          {
            clothName: "m_spurs_011",
            id: 102
          },
          {
            clothName: "m_spurs_011",
            id: 103
          }
        ],
        maxValue: 103,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "coats",
        clothRef: [
          {
            clothName: "m_coat_000",
            id: 1
          },
          {
            clothName: "m_coat_000",
            id: 2
          },
          {
            clothName: "m_coat_000",
            id: 3
          },
          {
            clothName: "m_coat_000",
            id: 4
          },
          {
            clothName: "m_coat_000",
            id: 5
          },
          {
            clothName: "m_coat_000",
            id: 6
          },
          {
            clothName: "m_coat_000",
            id: 7
          },
          {
            clothName: "m_coat_000",
            id: 8
          },
          {
            clothName: "m_coat_000",
            id: 9
          },
          {
            clothName: "m_coat_000",
            id: 10
          },
          {
            clothName: "m_coat_001",
            id: 11
          },
          {
            clothName: "m_coat_001",
            id: 12
          },
          {
            clothName: "m_coat_001",
            id: 13
          },
          {
            clothName: "m_coat_001",
            id: 14
          },
          {
            clothName: "m_coat_001",
            id: 15
          },
          {
            clothName: "m_coat_001",
            id: 16
          },
          {
            clothName: "m_coat_001",
            id: 17
          },
          {
            clothName: "m_coat_001",
            id: 18
          },
          {
            clothName: "m_coat_001",
            id: 19
          },
          {
            clothName: "m_coat_001",
            id: 20
          },
          {
            clothName: "m_coat_002",
            id: 21
          },
          {
            clothName: "m_coat_002",
            id: 22
          },
          {
            clothName: "m_coat_002",
            id: 23
          },
          {
            clothName: "m_coat_002",
            id: 24
          },
          {
            clothName: "m_coat_002",
            id: 25
          },
          {
            clothName: "m_coat_002",
            id: 26
          },
          {
            clothName: "m_coat_002",
            id: 27
          },
          {
            clothName: "m_coat_002",
            id: 28
          },
          {
            clothName: "m_coat_002",
            id: 29
          },
          {
            clothName: "m_coat_002",
            id: 30
          },
          {
            clothName: "m_coat_002",
            id: 31
          },
          {
            clothName: "m_coat_003",
            id: 32
          },
          {
            clothName: "m_coat_003",
            id: 33
          },
          {
            clothName: "m_coat_003",
            id: 34
          },
          {
            clothName: "m_coat_003",
            id: 35
          },
          {
            clothName: "m_coat_003",
            id: 36
          },
          {
            clothName: "m_coat_003",
            id: 37
          },
          {
            clothName: "m_coat_003",
            id: 38
          },
          {
            clothName: "m_coat_003",
            id: 39
          },
          {
            clothName: "m_coat_003",
            id: 40
          },
          {
            clothName: "m_coat_003",
            id: 41
          },
          {
            clothName: "m_coat_004",
            id: 42
          },
          {
            clothName: "m_coat_004",
            id: 43
          },
          {
            clothName: "m_coat_004",
            id: 44
          },
          {
            clothName: "m_coat_004",
            id: 45
          },
          {
            clothName: "m_coat_004",
            id: 46
          },
          {
            clothName: "m_coat_004",
            id: 47
          },
          {
            clothName: "m_coat_004",
            id: 48
          },
          {
            clothName: "m_coat_004",
            id: 49
          },
          {
            clothName: "m_coat_004",
            id: 50
          },
          {
            clothName: "m_coat_004",
            id: 51
          },
          {
            clothName: "m_coat_005",
            id: 52
          },
          {
            clothName: "m_coat_005",
            id: 53
          },
          {
            clothName: "m_coat_005",
            id: 54
          },
          {
            clothName: "m_coat_005",
            id: 55
          },
          {
            clothName: "m_coat_005",
            id: 56
          },
          {
            clothName: "m_coat_005",
            id: 57
          },
          {
            clothName: "m_coat_005",
            id: 58
          },
          {
            clothName: "m_coat_005",
            id: 59
          },
          {
            clothName: "m_coat_005",
            id: 60
          },
          {
            clothName: "m_coat_005",
            id: 61
          },
          {
            clothName: "m_coat_006",
            id: 62
          },
          {
            clothName: "m_coat_006",
            id: 63
          },
          {
            clothName: "m_coat_006",
            id: 64
          },
          {
            clothName: "m_coat_006",
            id: 65
          },
          {
            clothName: "m_coat_006",
            id: 66
          },
          {
            clothName: "m_coat_006",
            id: 67
          },
          {
            clothName: "m_coat_006",
            id: 68
          },
          {
            clothName: "m_coat_006",
            id: 69
          },
          {
            clothName: "m_coat_006",
            id: 70
          },
          {
            clothName: "m_coat_006",
            id: 71
          },
          {
            clothName: "m_coat_007",
            id: 72
          },
          {
            clothName: "m_coat_007",
            id: 73
          },
          {
            clothName: "m_coat_007",
            id: 74
          },
          {
            clothName: "m_coat_007",
            id: 75
          },
          {
            clothName: "m_coat_007",
            id: 76
          },
          {
            clothName: "m_coat_007",
            id: 77
          },
          {
            clothName: "m_coat_007",
            id: 78
          },
          {
            clothName: "m_coat_007",
            id: 79
          },
          {
            clothName: "m_coat_007",
            id: 80
          },
          {
            clothName: "m_coat_007",
            id: 81
          },
          {
            clothName: "m_coat_008",
            id: 82
          },
          {
            clothName: "m_coat_008",
            id: 83
          },
          {
            clothName: "m_coat_008",
            id: 84
          },
          {
            clothName: "m_coat_008",
            id: 85
          },
          {
            clothName: "m_coat_008",
            id: 86
          },
          {
            clothName: "m_coat_008",
            id: 87
          },
          {
            clothName: "m_coat_008",
            id: 88
          },
          {
            clothName: "m_coat_008",
            id: 89
          },
          {
            clothName: "m_coat_008",
            id: 90
          },
          {
            clothName: "m_coat_008",
            id: 91
          },
          {
            clothName: "m_coat_009",
            id: 92
          },
          {
            clothName: "m_coat_009",
            id: 93
          },
          {
            clothName: "m_coat_009",
            id: 94
          },
          {
            clothName: "m_coat_009",
            id: 95
          },
          {
            clothName: "m_coat_009",
            id: 96
          },
          {
            clothName: "m_coat_009",
            id: 97
          },
          {
            clothName: "m_coat_009",
            id: 98
          },
          {
            clothName: "m_coat_009",
            id: 99
          },
          {
            clothName: "m_coat_009",
            id: 100
          },
          {
            clothName: "m_coat_009",
            id: 101
          },
          {
            clothName: "m_coat_010",
            id: 102
          },
          {
            clothName: "m_coat_010",
            id: 103
          },
          {
            clothName: "m_coat_010",
            id: 104
          },
          {
            clothName: "m_coat_010",
            id: 105
          },
          {
            clothName: "m_coat_010",
            id: 106
          },
          {
            clothName: "m_coat_010",
            id: 107
          },
          {
            clothName: "m_coat_010",
            id: 108
          },
          {
            clothName: "m_coat_010",
            id: 109
          },
          {
            clothName: "m_coat_010",
            id: 110
          },
          {
            clothName: "m_coat_010",
            id: 111
          },
          {
            clothName: "m_coat_011",
            id: 112
          },
          {
            clothName: "m_coat_011",
            id: 113
          },
          {
            clothName: "m_coat_011",
            id: 114
          },
          {
            clothName: "m_coat_011",
            id: 115
          },
          {
            clothName: "m_coat_011",
            id: 116
          },
          {
            clothName: "m_coat_011",
            id: 117
          },
          {
            clothName: "m_coat_011",
            id: 118
          },
          {
            clothName: "m_coat_011",
            id: 119
          },
          {
            clothName: "m_coat_011",
            id: 120
          },
          {
            clothName: "m_coat_011",
            id: 121
          },
          {
            clothName: "m_coat_012",
            id: 122
          },
          {
            clothName: "m_coat_012",
            id: 123
          },
          {
            clothName: "m_coat_012",
            id: 124
          },
          {
            clothName: "m_coat_012",
            id: 125
          },
          {
            clothName: "m_coat_012",
            id: 126
          },
          {
            clothName: "m_coat_012",
            id: 127
          },
          {
            clothName: "m_coat_012",
            id: 128
          },
          {
            clothName: "m_coat_012",
            id: 129
          },
          {
            clothName: "m_coat_012",
            id: 130
          },
          {
            clothName: "m_coat_012",
            id: 131
          },
          {
            clothName: "m_coat_013",
            id: 132
          },
          {
            clothName: "m_coat_013",
            id: 133
          },
          {
            clothName: "m_coat_013",
            id: 134
          },
          {
            clothName: "m_coat_013",
            id: 135
          },
          {
            clothName: "m_coat_013",
            id: 136
          },
          {
            clothName: "m_coat_013",
            id: 137
          },
          {
            clothName: "m_coat_013",
            id: 138
          },
          {
            clothName: "m_coat_013",
            id: 139
          },
          {
            clothName: "m_coat_013",
            id: 140
          },
          {
            clothName: "m_coat_013",
            id: 141
          },
          {
            clothName: "m_coat_014",
            id: 142
          },
          {
            clothName: "m_coat_014",
            id: 143
          },
          {
            clothName: "m_coat_014",
            id: 144
          },
          {
            clothName: "m_coat_014",
            id: 145
          },
          {
            clothName: "m_coat_014",
            id: 146
          },
          {
            clothName: "m_coat_014",
            id: 147
          },
          {
            clothName: "m_coat_014",
            id: 148
          },
          {
            clothName: "m_coat_014",
            id: 149
          },
          {
            clothName: "m_coat_014",
            id: 150
          },
          {
            clothName: "m_coat_014",
            id: 151
          },
          {
            clothName: "m_coat_018",
            id: 152
          },
          {
            clothName: "m_coat_018",
            id: 153
          },
          {
            clothName: "m_coat_018",
            id: 154
          },
          {
            clothName: "m_coat_018",
            id: 155
          },
          {
            clothName: "m_coat_018",
            id: 156
          },
          {
            clothName: "m_coat_018",
            id: 157
          },
          {
            clothName: "m_coat_018",
            id: 158
          },
          {
            clothName: "m_coat_018",
            id: 159
          },
          {
            clothName: "m_coat_018",
            id: 160
          },
          {
            clothName: "m_coat_018",
            id: 161
          },
          {
            clothName: "m_coat_021",
            id: 162
          },
          {
            clothName: "m_coat_021",
            id: 163
          },
          {
            clothName: "m_coat_021",
            id: 164
          },
          {
            clothName: "m_coat_021",
            id: 165
          },
          {
            clothName: "m_coat_021",
            id: 166
          },
          {
            clothName: "m_coat_021",
            id: 167
          },
          {
            clothName: "m_coat_021",
            id: 168
          },
          {
            clothName: "m_coat_021",
            id: 169
          },
          {
            clothName: "m_coat_021",
            id: 170
          },
          {
            clothName: "m_coat_021",
            id: 171
          },
          {
            clothName: "m_coat_022",
            id: 172
          },
          {
            clothName: "m_coat_022",
            id: 173
          },
          {
            clothName: "m_coat_022",
            id: 174
          },
          {
            clothName: "m_coat_022",
            id: 175
          },
          {
            clothName: "m_coat_022",
            id: 176
          },
          {
            clothName: "m_coat_022",
            id: 177
          },
          {
            clothName: "m_coat_022",
            id: 178
          },
          {
            clothName: "m_coat_022",
            id: 179
          },
          {
            clothName: "m_coat_022",
            id: 180
          },
          {
            clothName: "m_coat_022",
            id: 181
          },
          {
            clothName: "m_coat_023",
            id: 182
          },
          {
            clothName: "m_coat_023",
            id: 183
          },
          {
            clothName: "m_coat_023",
            id: 184
          },
          {
            clothName: "m_coat_023",
            id: 185
          },
          {
            clothName: "m_coat_023",
            id: 186
          },
          {
            clothName: "m_coat_023",
            id: 187
          },
          {
            clothName: "m_coat_023",
            id: 188
          },
          {
            clothName: "m_coat_023",
            id: 189
          },
          {
            clothName: "m_coat_023",
            id: 190
          },
          {
            clothName: "m_coat_023",
            id: 191
          },
          {
            clothName: "m_coat_023",
            id: 192
          },
          {
            clothName: "m_coat_023",
            id: 193
          },
          {
            clothName: "m_coat_023",
            id: 194
          },
          {
            clothName: "m_coat_023",
            id: 195
          },
          {
            clothName: "m_coat_027",
            id: 196
          },
          {
            clothName: "m_coat_027",
            id: 197
          },
          {
            clothName: "m_coat_027",
            id: 198
          },
          {
            clothName: "m_coat_027",
            id: 199
          },
          {
            clothName: "m_coat_027",
            id: 200
          },
          {
            clothName: "m_coat_027",
            id: 201
          },
          {
            clothName: "m_coat_027",
            id: 202
          },
          {
            clothName: "m_coat_027",
            id: 203
          },
          {
            clothName: "m_coat_027",
            id: 204
          },
          {
            clothName: "m_coat_027",
            id: 205
          },
          {
            clothName: "m_coat_028",
            id: 206
          },
          {
            clothName: "m_coat_028",
            id: 207
          },
          {
            clothName: "m_coat_028",
            id: 208
          },
          {
            clothName: "m_coat_028",
            id: 209
          },
          {
            clothName: "m_coat_028",
            id: 210
          },
          {
            clothName: "m_coat_028",
            id: 211
          },
          {
            clothName: "m_coat_028",
            id: 212
          },
          {
            clothName: "m_coat_028",
            id: 213
          },
          {
            clothName: "m_coat_028",
            id: 214
          },
          {
            clothName: "m_coat_028",
            id: 215
          },
          {
            clothName: "m_coat_030",
            id: 216
          },
          {
            clothName: "m_coat_030",
            id: 217
          },
          {
            clothName: "m_coat_030",
            id: 218
          },
          {
            clothName: "m_coat_030",
            id: 219
          },
          {
            clothName: "m_coat_030",
            id: 220
          },
          {
            clothName: "m_coat_030",
            id: 221
          },
          {
            clothName: "m_coat_030",
            id: 222
          },
          {
            clothName: "m_coat_030",
            id: 223
          },
          {
            clothName: "m_coat_030",
            id: 224
          },
          {
            clothName: "m_coat_030",
            id: 225
          },
          {
            clothName: "m_coat_031",
            id: 226
          },
          {
            clothName: "m_coat_031",
            id: 227
          },
          {
            clothName: "m_coat_031",
            id: 228
          },
          {
            clothName: "m_coat_031",
            id: 229
          },
          {
            clothName: "m_coat_031",
            id: 230
          },
          {
            clothName: "m_coat_031",
            id: 231
          },
          {
            clothName: "m_coat_031",
            id: 232
          },
          {
            clothName: "m_coat_031",
            id: 233
          },
          {
            clothName: "m_coat_031",
            id: 234
          },
          {
            clothName: "m_coat_031",
            id: 235
          },
          {
            clothName: "m_coat_032",
            id: 236
          },
          {
            clothName: "m_coat_032",
            id: 237
          },
          {
            clothName: "m_coat_032",
            id: 238
          },
          {
            clothName: "m_coat_032",
            id: 239
          },
          {
            clothName: "m_coat_032",
            id: 240
          },
          {
            clothName: "m_coat_032",
            id: 241
          },
          {
            clothName: "m_coat_032",
            id: 242
          },
          {
            clothName: "m_coat_032",
            id: 243
          },
          {
            clothName: "m_coat_032",
            id: 244
          },
          {
            clothName: "m_coat_032",
            id: 245
          },
          {
            clothName: "m_coat_200",
            id: 246
          },
          {
            clothName: "m_coat_200",
            id: 247
          },
          {
            clothName: "m_coat_200",
            id: 248
          },
          {
            clothName: "m_coat_200",
            id: 249
          },
          {
            clothName: "m_coat_200",
            id: 250
          },
          {
            clothName: "m_coat_200",
            id: 251
          },
          {
            clothName: "m_coat_200",
            id: 252
          },
          {
            clothName: "m_coat_200",
            id: 253
          },
          {
            clothName: "m_coat_200",
            id: 254
          },
          {
            clothName: "m_coat_200",
            id: 255
          },
          {
            clothName: "m_coat_201",
            id: 256
          },
          {
            clothName: "m_coat_201",
            id: 257
          },
          {
            clothName: "m_coat_201",
            id: 258
          },
          {
            clothName: "m_coat_201",
            id: 259
          },
          {
            clothName: "m_coat_201",
            id: 260
          },
          {
            clothName: "m_coat_201",
            id: 261
          },
          {
            clothName: "m_coat_201",
            id: 262
          },
          {
            clothName: "m_coat_201",
            id: 263
          },
          {
            clothName: "m_coat_201",
            id: 264
          },
          {
            clothName: "m_coat_201",
            id: 265
          },
          {
            clothName: "m_coat_202",
            id: 266
          },
          {
            clothName: "m_coat_202",
            id: 267
          },
          {
            clothName: "m_coat_202",
            id: 268
          },
          {
            clothName: "m_coat_202",
            id: 269
          },
          {
            clothName: "m_coat_202",
            id: 270
          },
          {
            clothName: "m_coat_202",
            id: 271
          },
          {
            clothName: "m_coat_202",
            id: 272
          },
          {
            clothName: "m_coat_202",
            id: 273
          },
          {
            clothName: "m_coat_202",
            id: 274
          },
          {
            clothName: "m_coat_202",
            id: 275
          },
          {
            clothName: "m_coat_203",
            id: 276
          },
          {
            clothName: "m_coat_203",
            id: 277
          },
          {
            clothName: "m_coat_203",
            id: 278
          },
          {
            clothName: "m_coat_203",
            id: 279
          },
          {
            clothName: "m_coat_203",
            id: 280
          },
          {
            clothName: "m_coat_203",
            id: 281
          },
          {
            clothName: "m_coat_203",
            id: 282
          },
          {
            clothName: "m_coat_203",
            id: 283
          },
          {
            clothName: "m_coat_203",
            id: 284
          },
          {
            clothName: "m_coat_203",
            id: 285
          },
          {
            clothName: "m_coat_207",
            id: 286
          },
          {
            clothName: "m_coat_207",
            id: 287
          },
          {
            clothName: "m_coat_207",
            id: 288
          },
          {
            clothName: "m_coat_207",
            id: 289
          },
          {
            clothName: "m_coat_207",
            id: 290
          },
          {
            clothName: "m_coat_207",
            id: 291
          },
          {
            clothName: "m_coat_207",
            id: 292
          },
          {
            clothName: "m_coat_207",
            id: 293
          },
          {
            clothName: "m_coat_207",
            id: 294
          },
          {
            clothName: "m_coat_207",
            id: 295
          },
          {
            clothName: "m_coat_208",
            id: 296
          },
          {
            clothName: "m_coat_208",
            id: 297
          },
          {
            clothName: "m_coat_208",
            id: 298
          },
          {
            clothName: "m_coat_208",
            id: 299
          },
          {
            clothName: "m_coat_208",
            id: 300
          },
          {
            clothName: "m_coat_208",
            id: 301
          },
          {
            clothName: "m_coat_208",
            id: 302
          },
          {
            clothName: "m_coat_208",
            id: 303
          },
          {
            clothName: "m_coat_208",
            id: 304
          },
          {
            clothName: "m_coat_208",
            id: 305
          },
          {
            clothName: "unknown_item",
            id: 306
          },
          {
            clothName: "unknown_item",
            id: 307
          },
          {
            clothName: "unknown_item",
            id: 308
          },
          {
            clothName: "unknown_item",
            id: 309
          },
          {
            clothName: "unknown_item",
            id: 310
          }
        ],
        maxValue: 310,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "coats_closed",
        clothRef: [
          {
            clothName: "m_closecoat_000",
            id: 1
          },
          {
            clothName: "m_closecoat_000",
            id: 2
          },
          {
            clothName: "m_closecoat_000",
            id: 3
          },
          {
            clothName: "m_closecoat_000",
            id: 4
          },
          {
            clothName: "m_closecoat_000",
            id: 5
          },
          {
            clothName: "m_closecoat_000",
            id: 6
          },
          {
            clothName: "m_closecoat_000",
            id: 7
          },
          {
            clothName: "m_closecoat_000",
            id: 8
          },
          {
            clothName: "m_closecoat_000",
            id: 9
          },
          {
            clothName: "m_closecoat_000",
            id: 10
          },
          {
            clothName: "m_closecoat_002",
            id: 11
          },
          {
            clothName: "m_closecoat_002",
            id: 12
          },
          {
            clothName: "m_closecoat_002",
            id: 13
          },
          {
            clothName: "m_closecoat_002",
            id: 14
          },
          {
            clothName: "m_closecoat_002",
            id: 15
          },
          {
            clothName: "m_closecoat_002",
            id: 16
          },
          {
            clothName: "m_closecoat_002",
            id: 17
          },
          {
            clothName: "m_closecoat_002",
            id: 18
          },
          {
            clothName: "m_closecoat_002",
            id: 19
          },
          {
            clothName: "m_closecoat_002",
            id: 20
          },
          {
            clothName: "m_closecoat_003",
            id: 21
          },
          {
            clothName: "m_closecoat_003",
            id: 22
          },
          {
            clothName: "m_closecoat_003",
            id: 23
          },
          {
            clothName: "m_closecoat_003",
            id: 24
          },
          {
            clothName: "m_closecoat_003",
            id: 25
          },
          {
            clothName: "m_closecoat_003",
            id: 26
          },
          {
            clothName: "m_closecoat_003",
            id: 27
          },
          {
            clothName: "m_closecoat_003",
            id: 28
          },
          {
            clothName: "m_closecoat_003",
            id: 29
          },
          {
            clothName: "m_closecoat_003",
            id: 30
          },
          {
            clothName: "m_closecoat_005",
            id: 31
          },
          {
            clothName: "m_closecoat_005",
            id: 32
          },
          {
            clothName: "m_closecoat_005",
            id: 33
          },
          {
            clothName: "m_closecoat_005",
            id: 34
          },
          {
            clothName: "m_closecoat_005",
            id: 35
          },
          {
            clothName: "m_closecoat_005",
            id: 36
          },
          {
            clothName: "m_closecoat_005",
            id: 37
          },
          {
            clothName: "m_closecoat_005",
            id: 38
          },
          {
            clothName: "m_closecoat_005",
            id: 39
          },
          {
            clothName: "m_closecoat_005",
            id: 40
          },
          {
            clothName: "m_closecoat_007",
            id: 41
          },
          {
            clothName: "m_closecoat_007",
            id: 42
          },
          {
            clothName: "m_closecoat_007",
            id: 43
          },
          {
            clothName: "m_closecoat_007",
            id: 44
          },
          {
            clothName: "m_closecoat_007",
            id: 45
          },
          {
            clothName: "m_closecoat_007",
            id: 46
          },
          {
            clothName: "m_closecoat_007",
            id: 47
          },
          {
            clothName: "m_closecoat_007",
            id: 48
          },
          {
            clothName: "m_closecoat_007",
            id: 49
          },
          {
            clothName: "m_closecoat_007",
            id: 50
          },
          {
            clothName: "m_closecoat_009",
            id: 51
          },
          {
            clothName: "m_closecoat_009",
            id: 52
          },
          {
            clothName: "m_closecoat_009",
            id: 53
          },
          {
            clothName: "m_closecoat_009",
            id: 54
          },
          {
            clothName: "m_closecoat_009",
            id: 55
          },
          {
            clothName: "m_closecoat_009",
            id: 56
          },
          {
            clothName: "m_closecoat_009",
            id: 57
          },
          {
            clothName: "m_closecoat_009",
            id: 58
          },
          {
            clothName: "m_closecoat_009",
            id: 59
          },
          {
            clothName: "m_closecoat_009",
            id: 60
          },
          {
            clothName: "unknown_item",
            id: 61
          }
        ],
        maxValue: 61,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "neckties",
        clothRef: [
          {
            clothName: "necktie_000",
            id: 1
          },
          {
            clothName: "necktie_000",
            id: 2
          },
          {
            clothName: "necktie_000",
            id: 3
          },
          {
            clothName: "necktie_000",
            id: 4
          },
          {
            clothName: "necktie_000",
            id: 5
          },
          {
            clothName: "necktie_000",
            id: 6
          },
          {
            clothName: "necktie_000",
            id: 7
          },
          {
            clothName: "necktie_000",
            id: 8
          },
          {
            clothName: "necktie_000",
            id: 9
          },
          {
            clothName: "necktie_000",
            id: 10
          },
          {
            clothName: "necktie_001",
            id: 11
          },
          {
            clothName: "necktie_001",
            id: 12
          },
          {
            clothName: "necktie_001",
            id: 13
          },
          {
            clothName: "necktie_001",
            id: 14
          },
          {
            clothName: "necktie_001",
            id: 15
          },
          {
            clothName: "necktie_001",
            id: 16
          },
          {
            clothName: "necktie_001",
            id: 17
          },
          {
            clothName: "necktie_001",
            id: 18
          },
          {
            clothName: "necktie_001",
            id: 19
          },
          {
            clothName: "necktie_001",
            id: 20
          },
          {
            clothName: "necktie_002",
            id: 21
          },
          {
            clothName: "necktie_002",
            id: 22
          },
          {
            clothName: "necktie_002",
            id: 23
          },
          {
            clothName: "necktie_002",
            id: 24
          },
          {
            clothName: "necktie_002",
            id: 25
          },
          {
            clothName: "necktie_002",
            id: 26
          },
          {
            clothName: "necktie_002",
            id: 27
          },
          {
            clothName: "necktie_002",
            id: 28
          },
          {
            clothName: "necktie_002",
            id: 29
          },
          {
            clothName: "necktie_002",
            id: 30
          },
          {
            clothName: "necktie_003",
            id: 31
          },
          {
            clothName: "necktie_003",
            id: 32
          },
          {
            clothName: "necktie_003",
            id: 33
          },
          {
            clothName: "necktie_003",
            id: 34
          },
          {
            clothName: "necktie_003",
            id: 35
          },
          {
            clothName: "necktie_003",
            id: 36
          },
          {
            clothName: "necktie_003",
            id: 37
          },
          {
            clothName: "necktie_003",
            id: 38
          },
          {
            clothName: "necktie_003",
            id: 39
          },
          {
            clothName: "necktie_003",
            id: 40
          },
          {
            clothName: "necktie_004",
            id: 41
          },
          {
            clothName: "necktie_004",
            id: 42
          },
          {
            clothName: "necktie_004",
            id: 43
          },
          {
            clothName: "necktie_004",
            id: 44
          },
          {
            clothName: "necktie_004",
            id: 45
          },
          {
            clothName: "necktie_004",
            id: 46
          },
          {
            clothName: "necktie_004",
            id: 47
          },
          {
            clothName: "necktie_004",
            id: 48
          },
          {
            clothName: "necktie_004",
            id: 49
          },
          {
            clothName: "necktie_004",
            id: 50
          },
          {
            clothName: "necktie_005",
            id: 51
          },
          {
            clothName: "necktie_005",
            id: 52
          },
          {
            clothName: "necktie_005",
            id: 53
          },
          {
            clothName: "necktie_005",
            id: 54
          },
          {
            clothName: "necktie_005",
            id: 55
          },
          {
            clothName: "necktie_005",
            id: 56
          },
          {
            clothName: "necktie_005",
            id: 57
          },
          {
            clothName: "necktie_005",
            id: 58
          },
          {
            clothName: "necktie_005",
            id: 59
          },
          {
            clothName: "necktie_005",
            id: 60
          },
          {
            clothName: "necktie_006",
            id: 61
          },
          {
            clothName: "necktie_006",
            id: 62
          },
          {
            clothName: "necktie_006",
            id: 63
          },
          {
            clothName: "necktie_006",
            id: 64
          },
          {
            clothName: "necktie_006",
            id: 65
          },
          {
            clothName: "necktie_006",
            id: 66
          },
          {
            clothName: "necktie_006",
            id: 67
          },
          {
            clothName: "necktie_006",
            id: 68
          },
          {
            clothName: "necktie_006",
            id: 69
          },
          {
            clothName: "necktie_006",
            id: 70
          },
          {
            clothName: "necktie_007",
            id: 71
          },
          {
            clothName: "necktie_007",
            id: 72
          },
          {
            clothName: "necktie_007",
            id: 73
          },
          {
            clothName: "necktie_007",
            id: 74
          },
          {
            clothName: "necktie_007",
            id: 75
          },
          {
            clothName: "necktie_007",
            id: 76
          },
          {
            clothName: "necktie_007",
            id: 77
          },
          {
            clothName: "necktie_007",
            id: 78
          },
          {
            clothName: "necktie_007",
            id: 79
          },
          {
            clothName: "necktie_007",
            id: 80
          },
          {
            clothName: "unknown_item",
            id: 81
          }
        ],
        maxValue: 81,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "suspenders",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "suspenders_000",
            id: 2
          },
          {
            clothName: "unknown_item",
            id: 3
          },
          {
            clothName: "suspenders_000",
            id: 4
          },
          {
            clothName: "suspenders_000",
            id: 5
          },
          {
            clothName: "suspenders_000",
            id: 6
          },
          {
            clothName: "suspenders_000",
            id: 7
          },
          {
            clothName: "suspenders_000",
            id: 8
          },
          {
            clothName: "suspenders_000",
            id: 9
          },
          {
            clothName: "suspenders_000",
            id: 10
          },
          {
            clothName: "suspenders_000",
            id: 11
          },
          {
            clothName: "suspenders_000",
            id: 12
          },
          {
            clothName: "suspenders_000",
            id: 13
          },
          {
            clothName: "suspenders_001",
            id: 14
          },
          {
            clothName: "suspenders_001",
            id: 15
          },
          {
            clothName: "suspenders_001",
            id: 16
          },
          {
            clothName: "suspenders_001",
            id: 17
          },
          {
            clothName: "suspenders_001",
            id: 18
          },
          {
            clothName: "suspenders_001",
            id: 19
          },
          {
            clothName: "suspenders_001",
            id: 20
          },
          {
            clothName: "suspenders_001",
            id: 21
          },
          {
            clothName: "suspenders_001",
            id: 22
          },
          {
            clothName: "suspenders_001",
            id: 23
          },
          {
            clothName: "suspenders_002",
            id: 24
          },
          {
            clothName: "suspenders_002",
            id: 25
          },
          {
            clothName: "suspenders_002",
            id: 26
          },
          {
            clothName: "suspenders_002",
            id: 27
          },
          {
            clothName: "suspenders_002",
            id: 28
          },
          {
            clothName: "suspenders_002",
            id: 29
          },
          {
            clothName: "suspenders_002",
            id: 30
          },
          {
            clothName: "suspenders_002",
            id: 31
          },
          {
            clothName: "suspenders_002",
            id: 32
          },
          {
            clothName: "suspenders_002",
            id: 33
          },
          {
            clothName: "suspenders_003",
            id: 34
          },
          {
            clothName: "suspenders_003",
            id: 35
          },
          {
            clothName: "suspenders_003",
            id: 36
          },
          {
            clothName: "suspenders_003",
            id: 37
          },
          {
            clothName: "suspenders_003",
            id: 38
          },
          {
            clothName: "suspenders_003",
            id: 39
          },
          {
            clothName: "suspenders_003",
            id: 40
          },
          {
            clothName: "suspenders_003",
            id: 41
          },
          {
            clothName: "suspenders_003",
            id: 42
          },
          {
            clothName: "suspenders_003",
            id: 43
          },
          {
            clothName: "suspenders_004",
            id: 44
          },
          {
            clothName: "suspenders_004",
            id: 45
          },
          {
            clothName: "suspenders_004",
            id: 46
          },
          {
            clothName: "suspenders_004",
            id: 47
          },
          {
            clothName: "suspenders_004",
            id: 48
          },
          {
            clothName: "suspenders_004",
            id: 49
          },
          {
            clothName: "suspenders_004",
            id: 50
          },
          {
            clothName: "suspenders_004",
            id: 51
          },
          {
            clothName: "suspenders_004",
            id: 52
          },
          {
            clothName: "suspenders_004",
            id: 53
          },
          {
            clothName: "m_suspenders_200",
            id: 54
          },
          {
            clothName: "m_suspenders_200",
            id: 55
          },
          {
            clothName: "m_suspenders_200",
            id: 56
          },
          {
            clothName: "m_suspenders_200",
            id: 57
          },
          {
            clothName: "m_suspenders_200",
            id: 58
          },
          {
            clothName: "m_suspenders_200",
            id: 59
          },
          {
            clothName: "m_suspenders_200",
            id: 60
          },
          {
            clothName: "m_suspenders_200",
            id: 61
          },
          {
            clothName: "m_suspenders_200",
            id: 62
          },
          {
            clothName: "m_suspenders_200",
            id: 63
          }
        ],
        maxValue: 63,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "chaps",
        clothRef: [
          {
            clothName: "m_chaps_000",
            id: 1
          },
          {
            clothName: "m_chaps_000",
            id: 2
          },
          {
            clothName: "m_chaps_000",
            id: 3
          },
          {
            clothName: "m_chaps_000",
            id: 4
          },
          {
            clothName: "m_chaps_000",
            id: 5
          },
          {
            clothName: "m_chaps_000",
            id: 6
          },
          {
            clothName: "m_chaps_000",
            id: 7
          },
          {
            clothName: "m_chaps_000",
            id: 8
          },
          {
            clothName: "m_chaps_000",
            id: 9
          },
          {
            clothName: "m_chaps_000",
            id: 10
          },
          {
            clothName: "m_chaps_001",
            id: 11
          },
          {
            clothName: "m_chaps_001",
            id: 12
          },
          {
            clothName: "m_chaps_001",
            id: 13
          },
          {
            clothName: "m_chaps_001",
            id: 14
          },
          {
            clothName: "m_chaps_001",
            id: 15
          },
          {
            clothName: "m_chaps_001",
            id: 16
          },
          {
            clothName: "m_chaps_001",
            id: 17
          },
          {
            clothName: "m_chaps_001",
            id: 18
          },
          {
            clothName: "m_chaps_001",
            id: 19
          },
          {
            clothName: "m_chaps_001",
            id: 20
          },
          {
            clothName: "m_chaps_001",
            id: 21
          },
          {
            clothName: "m_chaps_001",
            id: 22
          },
          {
            clothName: "m_chaps_001",
            id: 23
          },
          {
            clothName: "m_chaps_001",
            id: 24
          },
          {
            clothName: "m_chaps_002",
            id: 25
          },
          {
            clothName: "m_chaps_002",
            id: 26
          },
          {
            clothName: "m_chaps_002",
            id: 27
          },
          {
            clothName: "m_chaps_002",
            id: 28
          },
          {
            clothName: "m_chaps_002",
            id: 29
          },
          {
            clothName: "m_chaps_002",
            id: 30
          },
          {
            clothName: "m_chaps_002",
            id: 31
          },
          {
            clothName: "m_chaps_002",
            id: 32
          },
          {
            clothName: "m_chaps_002",
            id: 33
          },
          {
            clothName: "m_chaps_002",
            id: 34
          },
          {
            clothName: "m_chaps_003",
            id: 35
          },
          {
            clothName: "m_chaps_003",
            id: 36
          },
          {
            clothName: "m_chaps_003",
            id: 37
          },
          {
            clothName: "m_chaps_003",
            id: 38
          },
          {
            clothName: "m_chaps_003",
            id: 39
          },
          {
            clothName: "m_chaps_003",
            id: 40
          },
          {
            clothName: "m_chaps_003",
            id: 41
          },
          {
            clothName: "m_chaps_003",
            id: 42
          },
          {
            clothName: "m_chaps_003",
            id: 43
          },
          {
            clothName: "m_chaps_003",
            id: 44
          },
          {
            clothName: "m_chaps_004",
            id: 45
          },
          {
            clothName: "m_chaps_004",
            id: 46
          },
          {
            clothName: "m_chaps_004",
            id: 47
          },
          {
            clothName: "m_chaps_004",
            id: 48
          },
          {
            clothName: "m_chaps_004",
            id: 49
          },
          {
            clothName: "m_chaps_004",
            id: 50
          },
          {
            clothName: "m_chaps_004",
            id: 51
          },
          {
            clothName: "m_chaps_004",
            id: 52
          },
          {
            clothName: "m_chaps_004",
            id: 53
          },
          {
            clothName: "m_chaps_004",
            id: 54
          }
        ],
        maxValue: 54,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "eyewear",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "unknown_item",
            id: 2
          },
          {
            clothName: "unknown_item",
            id: 3
          },
          {
            clothName: "unknown_item",
            id: 4
          },
          {
            clothName: "unknown_item",
            id: 5
          },
          {
            clothName: "unknown_item",
            id: 6
          },
          {
            clothName: "unknown_item",
            id: 7
          },
          {
            clothName: "unknown_item",
            id: 8
          },
          {
            clothName: "unknown_item",
            id: 9
          },
          {
            clothName: "unknown_item",
            id: 10
          }
        ],
        maxValue: 10,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "neckwear",
        clothRef: [
          {
            clothName: "neckwear_000",
            id: 1
          },
          {
            clothName: "neckerchief_001",
            id: 2
          },
          {
            clothName: "neckerchief_001",
            id: 3
          },
          {
            clothName: "neckerchief_001",
            id: 4
          },
          {
            clothName: "neckerchief_001",
            id: 5
          },
          {
            clothName: "neckerchief_001",
            id: 6
          },
          {
            clothName: "neckerchief_001",
            id: 7
          },
          {
            clothName: "neckerchief_001",
            id: 8
          },
          {
            clothName: "neckerchief_001",
            id: 9
          },
          {
            clothName: "neckerchief_002",
            id: 10
          },
          {
            clothName: "neckerchief_002",
            id: 11
          },
          {
            clothName: "neckerchief_002",
            id: 12
          },
          {
            clothName: "neckerchief_002",
            id: 13
          },
          {
            clothName: "neckerchief_002",
            id: 14
          },
          {
            clothName: "neckerchief_002",
            id: 15
          },
          {
            clothName: "neckerchief_002",
            id: 16
          },
          {
            clothName: "neckerchief_002",
            id: 17
          },
          {
            clothName: "neckerchief_002",
            id: 18
          },
          {
            clothName: "neckerchief_002",
            id: 19
          },
          {
            clothName: "neckerchief_002",
            id: 20
          },
          {
            clothName: "neckerchief_002",
            id: 21
          },
          {
            clothName: "neckerchief_002",
            id: 22
          },
          {
            clothName: "neckerchief_002",
            id: 23
          },
          {
            clothName: "neckerchief_002",
            id: 24
          },
          {
            clothName: "neckerchief_002",
            id: 25
          },
          {
            clothName: "neckerchief_002",
            id: 26
          },
          {
            clothName: "neckerchief_002",
            id: 27
          },
          {
            clothName: "neckerchief_002",
            id: 28
          },
          {
            clothName: "neckerchief_002",
            id: 29
          },
          {
            clothName: "neckerchief_002",
            id: 30
          },
          {
            clothName: "neckerchief_002",
            id: 31
          },
          {
            clothName: "neckerchief_002",
            id: 32
          },
          {
            clothName: "neckerchief_002",
            id: 33
          },
          {
            clothName: "neckerchief_002",
            id: 34
          },
          {
            clothName: "neckerchief_002",
            id: 35
          },
          {
            clothName: "neckerchief_003",
            id: 36
          },
          {
            clothName: "neckerchief_003",
            id: 37
          },
          {
            clothName: "neckerchief_003",
            id: 38
          },
          {
            clothName: "neckerchief_003",
            id: 39
          },
          {
            clothName: "neckerchief_003",
            id: 40
          },
          {
            clothName: "neckerchief_003",
            id: 41
          },
          {
            clothName: "neckerchief_003",
            id: 42
          },
          {
            clothName: "neckerchief_003",
            id: 43
          },
          {
            clothName: "neckerchief_003",
            id: 44
          },
          {
            clothName: "neckerchief_003",
            id: 45
          },
          {
            clothName: "neckerchief_003",
            id: 46
          },
          {
            clothName: "neckerchief_003",
            id: 47
          },
          {
            clothName: "neckerchief_003",
            id: 48
          },
          {
            clothName: "neckerchief_003",
            id: 49
          },
          {
            clothName: "neckerchief_003",
            id: 50
          },
          {
            clothName: "neckerchief_003",
            id: 51
          },
          {
            clothName: "neckerchief_003",
            id: 52
          },
          {
            clothName: "neckerchief_003",
            id: 53
          },
          {
            clothName: "neckerchief_003",
            id: 54
          },
          {
            clothName: "neckerchief_003",
            id: 55
          },
          {
            clothName: "neckerchief_003",
            id: 56
          },
          {
            clothName: "neckerchief_003",
            id: 57
          },
          {
            clothName: "neckerchief_003",
            id: 58
          },
          {
            clothName: "neckerchief_003",
            id: 59
          },
          {
            clothName: "neckerchief_003",
            id: 60
          },
          {
            clothName: "neckerchief_003",
            id: 61
          },
          {
            clothName: "neckwear_000",
            id: 62
          },
          {
            clothName: "neckwear_000",
            id: 63
          },
          {
            clothName: "neckwear_000",
            id: 64
          },
          {
            clothName: "neckwear_000",
            id: 65
          },
          {
            clothName: "neckwear_000",
            id: 66
          },
          {
            clothName: "neckwear_000",
            id: 67
          },
          {
            clothName: "neckwear_000",
            id: 68
          },
          {
            clothName: "neckwear_000",
            id: 69
          },
          {
            clothName: "neckwear_001",
            id: 70
          },
          {
            clothName: "neckwear_001",
            id: 71
          },
          {
            clothName: "neckwear_001",
            id: 72
          },
          {
            clothName: "neckwear_001",
            id: 73
          },
          {
            clothName: "neckwear_001",
            id: 74
          },
          {
            clothName: "neckwear_001",
            id: 75
          },
          {
            clothName: "neckwear_001",
            id: 76
          },
          {
            clothName: "neckwear_001",
            id: 77
          },
          {
            clothName: "neckwear_002",
            id: 78
          },
          {
            clothName: "neckwear_002",
            id: 79
          },
          {
            clothName: "neckwear_002",
            id: 80
          },
          {
            clothName: "neckwear_002",
            id: 81
          },
          {
            clothName: "neckwear_002",
            id: 82
          },
          {
            clothName: "neckwear_002",
            id: 83
          },
          {
            clothName: "neckwear_002",
            id: 84
          },
          {
            clothName: "neckwear_002",
            id: 85
          },
          {
            clothName: "m_neckwear_200",
            id: 86
          },
          {
            clothName: "m_neckwear_200",
            id: 87
          },
          {
            clothName: "m_neckwear_200",
            id: 88
          },
          {
            clothName: "m_neckwear_200",
            id: 89
          },
          {
            clothName: "m_neckwear_200",
            id: 90
          },
          {
            clothName: "m_neckwear_200",
            id: 91
          },
          {
            clothName: "m_neckwear_200",
            id: 92
          },
          {
            clothName: "m_neckwear_200",
            id: 93
          },
          {
            clothName: "neckwear_000",
            id: 94
          },
          {
            clothName: "unknown_item",
            id: 95
          },
          {
            clothName: "neckerchief_000",
            id: 96
          },
          {
            clothName: "neckerchief_001",
            id: 97
          },
          {
            clothName: "neckwear_000",
            id: 98
          },
          {
            clothName: "unknown_item",
            id: 99
          }
        ],
        maxValue: 99,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "gunbelts",
        clothRef: [
          {
            clothName: "gunbelt_000",
            id: 1
          },
          {
            clothName: "gunbelt_000",
            id: 2
          },
          {
            clothName: "gunbelt_000",
            id: 3
          },
          {
            clothName: "gunbelt_000",
            id: 4
          },
          {
            clothName: "gunbelt_001",
            id: 5
          },
          {
            clothName: "gunbelt_001",
            id: 6
          },
          {
            clothName: "gunbelt_001",
            id: 7
          },
          {
            clothName: "gunbelt_001",
            id: 8
          },
          {
            clothName: "gunbelt_002",
            id: 9
          },
          {
            clothName: "gunbelt_002",
            id: 10
          },
          {
            clothName: "gunbelt_002",
            id: 11
          },
          {
            clothName: "gunbelt_002",
            id: 12
          },
          {
            clothName: "gunbelt_003",
            id: 13
          },
          {
            clothName: "gunbelt_003",
            id: 14
          },
          {
            clothName: "gunbelt_003",
            id: 15
          },
          {
            clothName: "gunbelt_003",
            id: 16
          },
          {
            clothName: "gunbelt_004",
            id: 17
          },
          {
            clothName: "gunbelt_004",
            id: 18
          },
          {
            clothName: "gunbelt_004",
            id: 19
          },
          {
            clothName: "gunbelt_004",
            id: 20
          },
          {
            clothName: "gunbelt_005",
            id: 21
          },
          {
            clothName: "gunbelt_005",
            id: 22
          },
          {
            clothName: "gunbelt_005",
            id: 23
          },
          {
            clothName: "gunbelt_005",
            id: 24
          },
          {
            clothName: "gunbelt_006",
            id: 25
          },
          {
            clothName: "gunbelt_006",
            id: 26
          },
          {
            clothName: "gunbelt_006",
            id: 27
          },
          {
            clothName: "gunbelt_006",
            id: 28
          },
          {
            clothName: "gunbelt_007",
            id: 29
          },
          {
            clothName: "gunbelt_007",
            id: 30
          },
          {
            clothName: "gunbelt_007",
            id: 31
          },
          {
            clothName: "gunbelt_007",
            id: 32
          },
          {
            clothName: "gunbelt_008",
            id: 33
          },
          {
            clothName: "gunbelt_008",
            id: 34
          },
          {
            clothName: "gunbelt_008",
            id: 35
          },
          {
            clothName: "gunbelt_008",
            id: 36
          },
          {
            clothName: "gunbelt_009",
            id: 37
          },
          {
            clothName: "gunbelt_009",
            id: 38
          },
          {
            clothName: "gunbelt_009",
            id: 39
          },
          {
            clothName: "gunbelt_009",
            id: 40
          },
          {
            clothName: "gunbelt_010",
            id: 41
          },
          {
            clothName: "gunbelt_010",
            id: 42
          },
          {
            clothName: "gunbelt_010",
            id: 43
          },
          {
            clothName: "gunbelt_010",
            id: 44
          },
          {
            clothName: "gunbelt_014",
            id: 45
          },
          {
            clothName: "gunbelt_014",
            id: 46
          },
          {
            clothName: "gunbelt_014",
            id: 47
          },
          {
            clothName: "gunbelt_014",
            id: 48
          },
          {
            clothName: "gunbelt_015",
            id: 49
          },
          {
            clothName: "gunbelt_015",
            id: 50
          },
          {
            clothName: "gunbelt_015",
            id: 51
          },
          {
            clothName: "gunbelt_015",
            id: 52
          },
          {
            clothName: "gunbelt_000",
            id: 53
          }
        ],
        maxValue: 53,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "loadouts",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          }
        ],
        maxValue: 1,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "shirts_full",
        clothRef: [
          {
            clothName: "shirt_000",
            id: 1
          },
          {
            clothName: "shirt_000",
            id: 2
          },
          {
            clothName: "unknown_item",
            id: 3
          },
          {
            clothName: "shirt_000",
            id: 4
          },
          {
            clothName: "unknown_item",
            id: 5
          },
          {
            clothName: "shirt_000",
            id: 6
          },
          {
            clothName: "shirt_000",
            id: 7
          },
          {
            clothName: "shirt_000",
            id: 8
          },
          {
            clothName: "shirt_000",
            id: 9
          },
          {
            clothName: "shirt_000",
            id: 10
          },
          {
            clothName: "shirt_000",
            id: 11
          },
          {
            clothName: "shirt_000",
            id: 12
          },
          {
            clothName: "shirt_000",
            id: 13
          },
          {
            clothName: "shirt_000",
            id: 14
          },
          {
            clothName: "shirt_000",
            id: 15
          },
          {
            clothName: "shirt_000",
            id: 16
          },
          {
            clothName: "shirt_000",
            id: 17
          },
          {
            clothName: "shirt_000",
            id: 18
          },
          {
            clothName: "shirt_000",
            id: 19
          },
          {
            clothName: "shirt_000",
            id: 20
          },
          {
            clothName: "shirt_000",
            id: 21
          },
          {
            clothName: "shirt_000",
            id: 22
          },
          {
            clothName: "shirt_000",
            id: 23
          },
          {
            clothName: "shirt_000",
            id: 24
          },
          {
            clothName: "shirt_000",
            id: 25
          },
          {
            clothName: "shirt_000",
            id: 26
          },
          {
            clothName: "shirt_000",
            id: 27
          },
          {
            clothName: "shirt_000",
            id: 28
          },
          {
            clothName: "shirt_000",
            id: 29
          },
          {
            clothName: "shirt_000",
            id: 30
          },
          {
            clothName: "shirt_000",
            id: 31
          },
          {
            clothName: "shirt_000",
            id: 32
          },
          {
            clothName: "shirt_000",
            id: 33
          },
          {
            clothName: "shirt_000",
            id: 34
          },
          {
            clothName: "shirt_000",
            id: 35
          },
          {
            clothName: "m_shirt_001",
            id: 36
          },
          {
            clothName: "m_shirt_001",
            id: 37
          },
          {
            clothName: "m_shirt_001",
            id: 38
          },
          {
            clothName: "m_shirt_001",
            id: 39
          },
          {
            clothName: "m_shirt_001",
            id: 40
          },
          {
            clothName: "m_shirt_001",
            id: 41
          },
          {
            clothName: "m_shirt_001",
            id: 42
          },
          {
            clothName: "m_shirt_001",
            id: 43
          },
          {
            clothName: "m_shirt_001",
            id: 44
          },
          {
            clothName: "m_shirt_001",
            id: 45
          },
          {
            clothName: "m_shirt_001",
            id: 46
          },
          {
            clothName: "m_shirt_001",
            id: 47
          },
          {
            clothName: "m_shirt_001",
            id: 48
          },
          {
            clothName: "m_shirt_001",
            id: 49
          },
          {
            clothName: "m_shirt_001",
            id: 50
          },
          {
            clothName: "m_shirt_001",
            id: 51
          },
          {
            clothName: "m_shirt_001",
            id: 52
          },
          {
            clothName: "m_shirt_001",
            id: 53
          },
          {
            clothName: "m_shirt_001",
            id: 54
          },
          {
            clothName: "m_shirt_001",
            id: 55
          },
          {
            clothName: "m_shirt_002",
            id: 56
          },
          {
            clothName: "m_shirt_002",
            id: 57
          },
          {
            clothName: "m_shirt_002",
            id: 58
          },
          {
            clothName: "m_shirt_002",
            id: 59
          },
          {
            clothName: "m_shirt_002",
            id: 60
          },
          {
            clothName: "m_shirt_002",
            id: 61
          },
          {
            clothName: "m_shirt_002",
            id: 62
          },
          {
            clothName: "m_shirt_002",
            id: 63
          },
          {
            clothName: "m_shirt_002",
            id: 64
          },
          {
            clothName: "m_shirt_002",
            id: 65
          },
          {
            clothName: "m_shirt_002",
            id: 66
          },
          {
            clothName: "m_shirt_002",
            id: 67
          },
          {
            clothName: "m_shirt_002",
            id: 68
          },
          {
            clothName: "m_shirt_002",
            id: 69
          },
          {
            clothName: "m_shirt_002",
            id: 70
          },
          {
            clothName: "m_shirt_002",
            id: 71
          },
          {
            clothName: "m_shirt_002",
            id: 72
          },
          {
            clothName: "m_shirt_002",
            id: 73
          },
          {
            clothName: "m_shirt_002",
            id: 74
          },
          {
            clothName: "m_shirt_002",
            id: 75
          },
          {
            clothName: "m_shirt_004",
            id: 76
          },
          {
            clothName: "m_shirt_004",
            id: 77
          },
          {
            clothName: "m_shirt_004",
            id: 78
          },
          {
            clothName: "m_shirt_004",
            id: 79
          },
          {
            clothName: "m_shirt_004",
            id: 80
          },
          {
            clothName: "m_shirt_004",
            id: 81
          },
          {
            clothName: "m_shirt_004",
            id: 82
          },
          {
            clothName: "m_shirt_004",
            id: 83
          },
          {
            clothName: "m_shirt_004",
            id: 84
          },
          {
            clothName: "m_shirt_004",
            id: 85
          },
          {
            clothName: "m_shirt_004",
            id: 86
          },
          {
            clothName: "m_shirt_004",
            id: 87
          },
          {
            clothName: "m_shirt_004",
            id: 88
          },
          {
            clothName: "m_shirt_004",
            id: 89
          },
          {
            clothName: "m_shirt_004",
            id: 90
          },
          {
            clothName: "m_shirt_004",
            id: 91
          },
          {
            clothName: "m_shirt_004",
            id: 92
          },
          {
            clothName: "m_shirt_004",
            id: 93
          },
          {
            clothName: "m_shirt_004",
            id: 94
          },
          {
            clothName: "m_shirt_004",
            id: 95
          },
          {
            clothName: "shirt_005",
            id: 96
          },
          {
            clothName: "shirt_005",
            id: 97
          },
          {
            clothName: "shirt_005",
            id: 98
          },
          {
            clothName: "shirt_005",
            id: 99
          },
          {
            clothName: "shirt_005",
            id: 100
          },
          {
            clothName: "shirt_005",
            id: 101
          },
          {
            clothName: "shirt_005",
            id: 102
          },
          {
            clothName: "shirt_005",
            id: 103
          },
          {
            clothName: "shirt_005",
            id: 104
          },
          {
            clothName: "shirt_005",
            id: 105
          },
          {
            clothName: "shirt_005",
            id: 106
          },
          {
            clothName: "shirt_005",
            id: 107
          },
          {
            clothName: "shirt_005",
            id: 108
          },
          {
            clothName: "shirt_005",
            id: 109
          },
          {
            clothName: "shirt_005",
            id: 110
          },
          {
            clothName: "shirt_005",
            id: 111
          },
          {
            clothName: "shirt_005",
            id: 112
          },
          {
            clothName: "shirt_005",
            id: 113
          },
          {
            clothName: "shirt_005",
            id: 114
          },
          {
            clothName: "shirt_005",
            id: 115
          },
          {
            clothName: "shirt_006",
            id: 116
          },
          {
            clothName: "shirt_006",
            id: 117
          },
          {
            clothName: "shirt_006",
            id: 118
          },
          {
            clothName: "shirt_006",
            id: 119
          },
          {
            clothName: "shirt_006",
            id: 120
          },
          {
            clothName: "shirt_006",
            id: 121
          },
          {
            clothName: "shirt_006",
            id: 122
          },
          {
            clothName: "shirt_006",
            id: 123
          },
          {
            clothName: "shirt_006",
            id: 124
          },
          {
            clothName: "shirt_006",
            id: 125
          },
          {
            clothName: "shirt_006",
            id: 126
          },
          {
            clothName: "shirt_006",
            id: 127
          },
          {
            clothName: "shirt_006",
            id: 128
          },
          {
            clothName: "shirt_006",
            id: 129
          },
          {
            clothName: "shirt_006",
            id: 130
          },
          {
            clothName: "shirt_006",
            id: 131
          },
          {
            clothName: "shirt_006",
            id: 132
          },
          {
            clothName: "shirt_006",
            id: 133
          },
          {
            clothName: "shirt_006",
            id: 134
          },
          {
            clothName: "shirt_006",
            id: 135
          },
          {
            clothName: "m_shirt_007",
            id: 136
          },
          {
            clothName: "m_shirt_007",
            id: 137
          },
          {
            clothName: "m_shirt_007",
            id: 138
          },
          {
            clothName: "m_shirt_007",
            id: 139
          },
          {
            clothName: "m_shirt_007",
            id: 140
          },
          {
            clothName: "m_shirt_007",
            id: 141
          },
          {
            clothName: "m_shirt_007",
            id: 142
          },
          {
            clothName: "m_shirt_007",
            id: 143
          },
          {
            clothName: "m_shirt_007",
            id: 144
          },
          {
            clothName: "m_shirt_007",
            id: 145
          },
          {
            clothName: "m_shirt_008",
            id: 146
          },
          {
            clothName: "m_shirt_008",
            id: 147
          },
          {
            clothName: "m_shirt_008",
            id: 148
          },
          {
            clothName: "m_shirt_008",
            id: 149
          },
          {
            clothName: "m_shirt_008",
            id: 150
          },
          {
            clothName: "m_shirt_008",
            id: 151
          },
          {
            clothName: "m_shirt_008",
            id: 152
          },
          {
            clothName: "m_shirt_008",
            id: 153
          },
          {
            clothName: "m_shirt_008",
            id: 154
          },
          {
            clothName: "m_shirt_008",
            id: 155
          },
          {
            clothName: "m_shirt_008",
            id: 156
          },
          {
            clothName: "m_shirt_008",
            id: 157
          },
          {
            clothName: "m_shirt_008",
            id: 158
          },
          {
            clothName: "m_shirt_008",
            id: 159
          },
          {
            clothName: "m_shirt_008",
            id: 160
          },
          {
            clothName: "m_shirt_008",
            id: 161
          },
          {
            clothName: "m_shirt_008",
            id: 162
          },
          {
            clothName: "m_shirt_008",
            id: 163
          },
          {
            clothName: "m_shirt_008",
            id: 164
          },
          {
            clothName: "m_shirt_008",
            id: 165
          },
          {
            clothName: "shirt_009",
            id: 166
          },
          {
            clothName: "shirt_009",
            id: 167
          },
          {
            clothName: "shirt_009",
            id: 168
          },
          {
            clothName: "shirt_009",
            id: 169
          },
          {
            clothName: "shirt_009",
            id: 170
          },
          {
            clothName: "shirt_009",
            id: 171
          },
          {
            clothName: "shirt_009",
            id: 172
          },
          {
            clothName: "shirt_009",
            id: 173
          },
          {
            clothName: "shirt_009",
            id: 174
          },
          {
            clothName: "shirt_009",
            id: 175
          },
          {
            clothName: "shirt_009",
            id: 176
          },
          {
            clothName: "shirt_009",
            id: 177
          },
          {
            clothName: "shirt_009",
            id: 178
          },
          {
            clothName: "shirt_009",
            id: 179
          },
          {
            clothName: "shirt_009",
            id: 180
          },
          {
            clothName: "shirt_009",
            id: 181
          },
          {
            clothName: "shirt_009",
            id: 182
          },
          {
            clothName: "shirt_009",
            id: 183
          },
          {
            clothName: "shirt_009",
            id: 184
          },
          {
            clothName: "shirt_009",
            id: 185
          },
          {
            clothName: "shirt_010",
            id: 186
          },
          {
            clothName: "shirt_010",
            id: 187
          },
          {
            clothName: "shirt_010",
            id: 188
          },
          {
            clothName: "shirt_010",
            id: 189
          },
          {
            clothName: "shirt_010",
            id: 190
          },
          {
            clothName: "shirt_010",
            id: 191
          },
          {
            clothName: "shirt_010",
            id: 192
          },
          {
            clothName: "shirt_010",
            id: 193
          },
          {
            clothName: "shirt_010",
            id: 194
          },
          {
            clothName: "shirt_010",
            id: 195
          },
          {
            clothName: "shirt_010",
            id: 196
          },
          {
            clothName: "shirt_010",
            id: 197
          },
          {
            clothName: "shirt_010",
            id: 198
          },
          {
            clothName: "shirt_010",
            id: 199
          },
          {
            clothName: "shirt_010",
            id: 200
          },
          {
            clothName: "shirt_010",
            id: 201
          },
          {
            clothName: "shirt_010",
            id: 202
          },
          {
            clothName: "shirt_010",
            id: 203
          },
          {
            clothName: "shirt_010",
            id: 204
          },
          {
            clothName: "shirt_010",
            id: 205
          },
          {
            clothName: "m_shirt_013",
            id: 206
          },
          {
            clothName: "m_shirt_013",
            id: 207
          },
          {
            clothName: "m_shirt_013",
            id: 208
          },
          {
            clothName: "m_shirt_013",
            id: 209
          },
          {
            clothName: "m_shirt_013",
            id: 210
          },
          {
            clothName: "m_shirt_013",
            id: 211
          },
          {
            clothName: "m_shirt_013",
            id: 212
          },
          {
            clothName: "m_shirt_013",
            id: 213
          },
          {
            clothName: "m_shirt_013",
            id: 214
          },
          {
            clothName: "m_shirt_013",
            id: 215
          },
          {
            clothName: "m_shirt_014",
            id: 216
          },
          {
            clothName: "m_shirt_014",
            id: 217
          },
          {
            clothName: "m_shirt_014",
            id: 218
          },
          {
            clothName: "m_shirt_014",
            id: 219
          },
          {
            clothName: "m_shirt_014",
            id: 220
          },
          {
            clothName: "m_shirt_014",
            id: 221
          },
          {
            clothName: "m_shirt_014",
            id: 222
          },
          {
            clothName: "m_shirt_014",
            id: 223
          },
          {
            clothName: "m_shirt_014",
            id: 224
          },
          {
            clothName: "m_shirt_014",
            id: 225
          },
          {
            clothName: "m_shirt_204",
            id: 226
          },
          {
            clothName: "m_shirt_204",
            id: 227
          },
          {
            clothName: "m_shirt_204",
            id: 228
          },
          {
            clothName: "m_shirt_204",
            id: 229
          },
          {
            clothName: "m_shirt_204",
            id: 230
          },
          {
            clothName: "m_shirt_204",
            id: 231
          },
          {
            clothName: "m_shirt_204",
            id: 232
          },
          {
            clothName: "m_shirt_204",
            id: 233
          },
          {
            clothName: "m_shirt_204",
            id: 234
          },
          {
            clothName: "m_shirt_204",
            id: 235
          },
          {
            clothName: "m_shirt_205",
            id: 236
          },
          {
            clothName: "m_shirt_205",
            id: 237
          },
          {
            clothName: "m_shirt_205",
            id: 238
          },
          {
            clothName: "m_shirt_205",
            id: 239
          },
          {
            clothName: "m_shirt_205",
            id: 240
          },
          {
            clothName: "m_shirt_205",
            id: 241
          },
          {
            clothName: "m_shirt_205",
            id: 242
          },
          {
            clothName: "m_shirt_205",
            id: 243
          },
          {
            clothName: "m_shirt_205",
            id: 244
          },
          {
            clothName: "m_shirt_205",
            id: 245
          },
          {
            clothName: "m_shirt_206",
            id: 246
          },
          {
            clothName: "m_shirt_206",
            id: 247
          },
          {
            clothName: "m_shirt_206",
            id: 248
          },
          {
            clothName: "m_shirt_206",
            id: 249
          },
          {
            clothName: "m_shirt_206",
            id: 250
          },
          {
            clothName: "m_shirt_206",
            id: 251
          },
          {
            clothName: "m_shirt_206",
            id: 252
          },
          {
            clothName: "m_shirt_206",
            id: 253
          },
          {
            clothName: "m_shirt_206",
            id: 254
          },
          {
            clothName: "m_shirt_206",
            id: 255
          },
          {
            clothName: "m_shirt_207",
            id: 256
          },
          {
            clothName: "m_shirt_207",
            id: 257
          },
          {
            clothName: "m_shirt_207",
            id: 258
          },
          {
            clothName: "m_shirt_207",
            id: 259
          },
          {
            clothName: "m_shirt_207",
            id: 260
          },
          {
            clothName: "m_shirt_207",
            id: 261
          },
          {
            clothName: "m_shirt_207",
            id: 262
          },
          {
            clothName: "m_shirt_207",
            id: 263
          },
          {
            clothName: "m_shirt_207",
            id: 264
          },
          {
            clothName: "m_shirt_207",
            id: 265
          },
          {
            clothName: "m_shirt_208",
            id: 266
          },
          {
            clothName: "m_shirt_208",
            id: 267
          },
          {
            clothName: "m_shirt_208",
            id: 268
          },
          {
            clothName: "m_shirt_208",
            id: 269
          },
          {
            clothName: "m_shirt_208",
            id: 270
          },
          {
            clothName: "m_shirt_208",
            id: 271
          },
          {
            clothName: "m_shirt_208",
            id: 272
          },
          {
            clothName: "m_shirt_208",
            id: 273
          },
          {
            clothName: "m_shirt_208",
            id: 274
          },
          {
            clothName: "m_shirt_208",
            id: 275
          },
          {
            clothName: "m_shirt_210",
            id: 276
          },
          {
            clothName: "m_shirt_210",
            id: 277
          },
          {
            clothName: "m_shirt_210",
            id: 278
          },
          {
            clothName: "m_shirt_210",
            id: 279
          },
          {
            clothName: "m_shirt_210",
            id: 280
          },
          {
            clothName: "m_shirt_210",
            id: 281
          },
          {
            clothName: "m_shirt_210",
            id: 282
          },
          {
            clothName: "m_shirt_210",
            id: 283
          },
          {
            clothName: "m_shirt_210",
            id: 284
          },
          {
            clothName: "m_shirt_210",
            id: 285
          },
          {
            clothName: "m_shirt_212",
            id: 286
          },
          {
            clothName: "m_shirt_212",
            id: 287
          },
          {
            clothName: "m_shirt_212",
            id: 288
          },
          {
            clothName: "m_shirt_212",
            id: 289
          },
          {
            clothName: "m_shirt_212",
            id: 290
          },
          {
            clothName: "m_shirt_212",
            id: 291
          },
          {
            clothName: "m_shirt_212",
            id: 292
          },
          {
            clothName: "m_shirt_212",
            id: 293
          },
          {
            clothName: "m_shirt_212",
            id: 294
          },
          {
            clothName: "m_shirt_212",
            id: 295
          },
          {
            clothName: "ust_000",
            id: 296
          },
          {
            clothName: "ust_000",
            id: 297
          },
          {
            clothName: "ust_000",
            id: 298
          },
          {
            clothName: "ust_000",
            id: 299
          },
          {
            clothName: "ust_000",
            id: 300
          },
          {
            clothName: "ust_000",
            id: 301
          },
          {
            clothName: "ust_000",
            id: 302
          },
          {
            clothName: "ust_000",
            id: 303
          },
          {
            clothName: "ust_000",
            id: 304
          },
          {
            clothName: "ust_000",
            id: 305
          },
          {
            clothName: "shirt_000",
            id: 306
          }
        ],
        maxValue: 306,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "jewelry_rings_left",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          }
        ],
        maxValue: 1,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "holsters_left",
        clothRef: [
          {
            clothName: "offhand_000",
            id: 1
          },
          {
            clothName: "offhand_000",
            id: 2
          },
          {
            clothName: "offhand_000",
            id: 3
          },
          {
            clothName: "offhand_000",
            id: 4
          },
          {
            clothName: "offhand_001",
            id: 5
          },
          {
            clothName: "offhand_001",
            id: 6
          },
          {
            clothName: "offhand_001",
            id: 7
          },
          {
            clothName: "offhand_001",
            id: 8
          },
          {
            clothName: "offhand_002",
            id: 9
          },
          {
            clothName: "offhand_002",
            id: 10
          },
          {
            clothName: "offhand_002",
            id: 11
          },
          {
            clothName: "offhand_002",
            id: 12
          },
          {
            clothName: "offhand_003",
            id: 13
          },
          {
            clothName: "offhand_003",
            id: 14
          },
          {
            clothName: "offhand_003",
            id: 15
          },
          {
            clothName: "offhand_003",
            id: 16
          },
          {
            clothName: "offhand_004",
            id: 17
          },
          {
            clothName: "offhand_004",
            id: 18
          },
          {
            clothName: "offhand_004",
            id: 19
          },
          {
            clothName: "offhand_004",
            id: 20
          },
          {
            clothName: "offhand_005",
            id: 21
          },
          {
            clothName: "offhand_005",
            id: 22
          },
          {
            clothName: "offhand_005",
            id: 23
          },
          {
            clothName: "offhand_005",
            id: 24
          },
          {
            clothName: "offhand_006",
            id: 25
          },
          {
            clothName: "offhand_006",
            id: 26
          },
          {
            clothName: "offhand_006",
            id: 27
          },
          {
            clothName: "offhand_006",
            id: 28
          },
          {
            clothName: "offhand_007",
            id: 29
          },
          {
            clothName: "offhand_007",
            id: 30
          },
          {
            clothName: "offhand_007",
            id: 31
          },
          {
            clothName: "offhand_007",
            id: 32
          },
          {
            clothName: "offhand_008",
            id: 33
          },
          {
            clothName: "offhand_008",
            id: 34
          },
          {
            clothName: "offhand_008",
            id: 35
          },
          {
            clothName: "offhand_008",
            id: 36
          },
          {
            clothName: "offhand_009",
            id: 37
          },
          {
            clothName: "offhand_009",
            id: 38
          },
          {
            clothName: "offhand_009",
            id: 39
          },
          {
            clothName: "offhand_009",
            id: 40
          },
          {
            clothName: "offhand_010",
            id: 41
          },
          {
            clothName: "offhand_010",
            id: 42
          },
          {
            clothName: "offhand_010",
            id: 43
          },
          {
            clothName: "offhand_010",
            id: 44
          },
          {
            clothName: "offhand_014",
            id: 45
          },
          {
            clothName: "offhand_014",
            id: 46
          },
          {
            clothName: "offhand_014",
            id: 47
          },
          {
            clothName: "offhand_014",
            id: 48
          },
          {
            clothName: "offhand_015",
            id: 49
          },
          {
            clothName: "offhand_015",
            id: 50
          },
          {
            clothName: "offhand_015",
            id: 51
          },
          {
            clothName: "offhand_015",
            id: 52
          }
        ],
        maxValue: 52,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "spats",
        clothRef: [
          {
            clothName: "spats_000",
            id: 1
          },
          {
            clothName: "spats_000",
            id: 2
          },
          {
            clothName: "spats_000",
            id: 3
          },
          {
            clothName: "spats_000",
            id: 4
          },
          {
            clothName: "spats_000",
            id: 5
          },
          {
            clothName: "spats_000",
            id: 6
          },
          {
            clothName: "spats_000",
            id: 7
          },
          {
            clothName: "spats_000",
            id: 8
          },
          {
            clothName: "spats_000",
            id: 9
          },
          {
            clothName: "spats_000",
            id: 10
          },
          {
            clothName: "spats_000",
            id: 11
          },
          {
            clothName: "spats_001",
            id: 12
          },
          {
            clothName: "spats_001",
            id: 13
          },
          {
            clothName: "spats_001",
            id: 14
          },
          {
            clothName: "spats_001",
            id: 15
          },
          {
            clothName: "spats_001",
            id: 16
          },
          {
            clothName: "spats_001",
            id: 17
          },
          {
            clothName: "spats_001",
            id: 18
          },
          {
            clothName: "spats_001",
            id: 19
          },
          {
            clothName: "spats_001",
            id: 20
          },
          {
            clothName: "spats_001",
            id: 21
          },
          {
            clothName: "spats_002",
            id: 22
          },
          {
            clothName: "spats_002",
            id: 23
          },
          {
            clothName: "spats_002",
            id: 24
          },
          {
            clothName: "spats_002",
            id: 25
          },
          {
            clothName: "spats_002",
            id: 26
          },
          {
            clothName: "spats_002",
            id: 27
          },
          {
            clothName: "spats_002",
            id: 28
          },
          {
            clothName: "spats_002",
            id: 29
          },
          {
            clothName: "spats_002",
            id: 30
          },
          {
            clothName: "spats_002",
            id: 31
          },
          {
            clothName: "m_spats_004",
            id: 32
          },
          {
            clothName: "m_spats_004",
            id: 33
          },
          {
            clothName: "m_spats_004",
            id: 34
          },
          {
            clothName: "m_spats_004",
            id: 35
          },
          {
            clothName: "m_spats_004",
            id: 36
          },
          {
            clothName: "m_spats_004",
            id: 37
          },
          {
            clothName: "m_spats_004",
            id: 38
          },
          {
            clothName: "m_spats_004",
            id: 39
          },
          {
            clothName: "m_spats_004",
            id: 40
          },
          {
            clothName: "m_spats_004",
            id: 41
          },
          {
            clothName: "spats_005",
            id: 42
          },
          {
            clothName: "spats_005",
            id: 43
          },
          {
            clothName: "spats_005",
            id: 44
          },
          {
            clothName: "spats_005",
            id: 45
          },
          {
            clothName: "spats_005",
            id: 46
          },
          {
            clothName: "spats_005",
            id: 47
          },
          {
            clothName: "spats_005",
            id: 48
          },
          {
            clothName: "spats_005",
            id: 49
          },
          {
            clothName: "spats_005",
            id: 50
          },
          {
            clothName: "spats_005",
            id: 51
          },
          {
            clothName: "m_spats_006",
            id: 52
          },
          {
            clothName: "m_spats_006",
            id: 53
          },
          {
            clothName: "m_spats_006",
            id: 54
          },
          {
            clothName: "m_spats_006",
            id: 55
          },
          {
            clothName: "m_spats_006",
            id: 56
          },
          {
            clothName: "m_spats_006",
            id: 57
          },
          {
            clothName: "m_spats_007",
            id: 58
          },
          {
            clothName: "m_spats_007",
            id: 59
          },
          {
            clothName: "m_spats_007",
            id: 60
          },
          {
            clothName: "m_spats_007",
            id: 61
          },
          {
            clothName: "m_spats_007",
            id: 62
          },
          {
            clothName: "m_spats_007",
            id: 63
          },
          {
            clothName: "m_spats_007",
            id: 64
          },
          {
            clothName: "m_spats_007",
            id: 65
          },
          {
            clothName: "m_spats_007",
            id: 66
          },
          {
            clothName: "m_spats_007",
            id: 67
          },
          {
            clothName: "m_spats_008",
            id: 68
          },
          {
            clothName: "m_spats_008",
            id: 69
          },
          {
            clothName: "m_spats_008",
            id: 70
          },
          {
            clothName: "m_spats_008",
            id: 71
          },
          {
            clothName: "m_spats_008",
            id: 72
          },
          {
            clothName: "m_spats_008",
            id: 73
          },
          {
            clothName: "m_spats_008",
            id: 74
          },
          {
            clothName: "m_spats_008",
            id: 75
          },
          {
            clothName: "m_spats_008",
            id: 76
          },
          {
            clothName: "m_spats_008",
            id: 77
          },
          {
            clothName: "spats_000",
            id: 78
          }
        ],
        maxValue: 78,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "vests",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "vest_000",
            id: 2
          },
          {
            clothName: "vest_000",
            id: 3
          },
          {
            clothName: "vest_000",
            id: 4
          },
          {
            clothName: "vest_000",
            id: 5
          },
          {
            clothName: "vest_000",
            id: 6
          },
          {
            clothName: "vest_000",
            id: 7
          },
          {
            clothName: "vest_000",
            id: 8
          },
          {
            clothName: "vest_000",
            id: 9
          },
          {
            clothName: "vest_000",
            id: 10
          },
          {
            clothName: "vest_000",
            id: 11
          },
          {
            clothName: "vest_000",
            id: 12
          },
          {
            clothName: "vest_000",
            id: 13
          },
          {
            clothName: "vest_001",
            id: 14
          },
          {
            clothName: "vest_001",
            id: 15
          },
          {
            clothName: "vest_001",
            id: 16
          },
          {
            clothName: "vest_001",
            id: 17
          },
          {
            clothName: "vest_001",
            id: 18
          },
          {
            clothName: "vest_001",
            id: 19
          },
          {
            clothName: "vest_001",
            id: 20
          },
          {
            clothName: "vest_001",
            id: 21
          },
          {
            clothName: "vest_001",
            id: 22
          },
          {
            clothName: "vest_001",
            id: 23
          },
          {
            clothName: "vest_002",
            id: 24
          },
          {
            clothName: "vest_002",
            id: 25
          },
          {
            clothName: "vest_002",
            id: 26
          },
          {
            clothName: "vest_002",
            id: 27
          },
          {
            clothName: "vest_002",
            id: 28
          },
          {
            clothName: "vest_002",
            id: 29
          },
          {
            clothName: "vest_002",
            id: 30
          },
          {
            clothName: "vest_002",
            id: 31
          },
          {
            clothName: "vest_002",
            id: 32
          },
          {
            clothName: "vest_002",
            id: 33
          },
          {
            clothName: "vest_003",
            id: 34
          },
          {
            clothName: "vest_003",
            id: 35
          },
          {
            clothName: "vest_003",
            id: 36
          },
          {
            clothName: "vest_003",
            id: 37
          },
          {
            clothName: "vest_003",
            id: 38
          },
          {
            clothName: "vest_003",
            id: 39
          },
          {
            clothName: "vest_003",
            id: 40
          },
          {
            clothName: "vest_003",
            id: 41
          },
          {
            clothName: "vest_003",
            id: 42
          },
          {
            clothName: "vest_003",
            id: 43
          },
          {
            clothName: "vest_004",
            id: 44
          },
          {
            clothName: "vest_004",
            id: 45
          },
          {
            clothName: "vest_004",
            id: 46
          },
          {
            clothName: "vest_004",
            id: 47
          },
          {
            clothName: "vest_004",
            id: 48
          },
          {
            clothName: "vest_004",
            id: 49
          },
          {
            clothName: "vest_004",
            id: 50
          },
          {
            clothName: "vest_004",
            id: 51
          },
          {
            clothName: "vest_004",
            id: 52
          },
          {
            clothName: "vest_004",
            id: 53
          },
          {
            clothName: "vest_005",
            id: 54
          },
          {
            clothName: "vest_005",
            id: 55
          },
          {
            clothName: "vest_005",
            id: 56
          },
          {
            clothName: "vest_005",
            id: 57
          },
          {
            clothName: "vest_005",
            id: 58
          },
          {
            clothName: "vest_005",
            id: 59
          },
          {
            clothName: "vest_005",
            id: 60
          },
          {
            clothName: "vest_005",
            id: 61
          },
          {
            clothName: "vest_005",
            id: 62
          },
          {
            clothName: "vest_005",
            id: 63
          },
          {
            clothName: "vest_006",
            id: 64
          },
          {
            clothName: "vest_006",
            id: 65
          },
          {
            clothName: "vest_006",
            id: 66
          },
          {
            clothName: "vest_006",
            id: 67
          },
          {
            clothName: "vest_006",
            id: 68
          },
          {
            clothName: "vest_006",
            id: 69
          },
          {
            clothName: "vest_006",
            id: 70
          },
          {
            clothName: "vest_006",
            id: 71
          },
          {
            clothName: "vest_006",
            id: 72
          },
          {
            clothName: "vest_006",
            id: 73
          },
          {
            clothName: "vest_008",
            id: 74
          },
          {
            clothName: "vest_008",
            id: 75
          },
          {
            clothName: "vest_008",
            id: 76
          },
          {
            clothName: "vest_008",
            id: 77
          },
          {
            clothName: "vest_008",
            id: 78
          },
          {
            clothName: "vest_008",
            id: 79
          },
          {
            clothName: "vest_008",
            id: 80
          },
          {
            clothName: "vest_008",
            id: 81
          },
          {
            clothName: "vest_008",
            id: 82
          },
          {
            clothName: "vest_008",
            id: 83
          },
          {
            clothName: "vest_010",
            id: 84
          },
          {
            clothName: "vest_010",
            id: 85
          },
          {
            clothName: "vest_010",
            id: 86
          },
          {
            clothName: "vest_010",
            id: 87
          },
          {
            clothName: "vest_010",
            id: 88
          },
          {
            clothName: "vest_010",
            id: 89
          },
          {
            clothName: "vest_010",
            id: 90
          },
          {
            clothName: "vest_010",
            id: 91
          },
          {
            clothName: "vest_010",
            id: 92
          },
          {
            clothName: "vest_010",
            id: 93
          },
          {
            clothName: "vest_010",
            id: 94
          },
          {
            clothName: "vest_010",
            id: 95
          },
          {
            clothName: "vest_010",
            id: 96
          },
          {
            clothName: "vest_010",
            id: 97
          },
          {
            clothName: "vest_010",
            id: 98
          },
          {
            clothName: "vest_010",
            id: 99
          },
          {
            clothName: "vest_010",
            id: 100
          },
          {
            clothName: "vest_010",
            id: 101
          },
          {
            clothName: "vest_010",
            id: 102
          },
          {
            clothName: "vest_010",
            id: 103
          },
          {
            clothName: "m_vest_014",
            id: 104
          },
          {
            clothName: "m_vest_014",
            id: 105
          },
          {
            clothName: "m_vest_014",
            id: 106
          },
          {
            clothName: "m_vest_014",
            id: 107
          },
          {
            clothName: "m_vest_014",
            id: 108
          },
          {
            clothName: "m_vest_014",
            id: 109
          },
          {
            clothName: "m_vest_014",
            id: 110
          },
          {
            clothName: "m_vest_014",
            id: 111
          },
          {
            clothName: "m_vest_014",
            id: 112
          },
          {
            clothName: "m_vest_014",
            id: 113
          },
          {
            clothName: "m_vest_014",
            id: 114
          },
          {
            clothName: "m_vest_014",
            id: 115
          },
          {
            clothName: "m_vest_014",
            id: 116
          },
          {
            clothName: "m_vest_014",
            id: 117
          },
          {
            clothName: "m_vest_014",
            id: 118
          },
          {
            clothName: "m_vest_015",
            id: 119
          },
          {
            clothName: "m_vest_015",
            id: 120
          },
          {
            clothName: "m_vest_015",
            id: 121
          },
          {
            clothName: "m_vest_015",
            id: 122
          },
          {
            clothName: "m_vest_015",
            id: 123
          },
          {
            clothName: "m_vest_016",
            id: 124
          },
          {
            clothName: "m_vest_016",
            id: 125
          },
          {
            clothName: "m_vest_016",
            id: 126
          },
          {
            clothName: "m_vest_016",
            id: 127
          },
          {
            clothName: "m_vest_016",
            id: 128
          },
          {
            clothName: "m_vest_016",
            id: 129
          },
          {
            clothName: "m_vest_016",
            id: 130
          },
          {
            clothName: "m_vest_016",
            id: 131
          },
          {
            clothName: "m_vest_016",
            id: 132
          },
          {
            clothName: "m_vest_016",
            id: 133
          },
          {
            clothName: "m_vest_017",
            id: 134
          },
          {
            clothName: "m_vest_017",
            id: 135
          },
          {
            clothName: "m_vest_017",
            id: 136
          },
          {
            clothName: "m_vest_017",
            id: 137
          },
          {
            clothName: "m_vest_017",
            id: 138
          },
          {
            clothName: "m_vest_017",
            id: 139
          },
          {
            clothName: "m_vest_017",
            id: 140
          },
          {
            clothName: "m_vest_017",
            id: 141
          },
          {
            clothName: "m_vest_017",
            id: 142
          },
          {
            clothName: "m_vest_017",
            id: 143
          },
          {
            clothName: "m_vest_018",
            id: 144
          },
          {
            clothName: "m_vest_018",
            id: 145
          },
          {
            clothName: "m_vest_018",
            id: 146
          },
          {
            clothName: "m_vest_018",
            id: 147
          },
          {
            clothName: "m_vest_018",
            id: 148
          },
          {
            clothName: "m_vest_018",
            id: 149
          },
          {
            clothName: "m_vest_018",
            id: 150
          },
          {
            clothName: "m_vest_018",
            id: 151
          },
          {
            clothName: "m_vest_018",
            id: 152
          },
          {
            clothName: "m_vest_018",
            id: 153
          },
          {
            clothName: "m_vest_200",
            id: 154
          },
          {
            clothName: "m_vest_200",
            id: 155
          },
          {
            clothName: "m_vest_200",
            id: 156
          },
          {
            clothName: "m_vest_200",
            id: 157
          },
          {
            clothName: "m_vest_200",
            id: 158
          },
          {
            clothName: "m_vest_200",
            id: 159
          },
          {
            clothName: "m_vest_200",
            id: 160
          },
          {
            clothName: "m_vest_200",
            id: 161
          },
          {
            clothName: "m_vest_200",
            id: 162
          },
          {
            clothName: "m_vest_200",
            id: 163
          },
          {
            clothName: "m_vest_201",
            id: 164
          },
          {
            clothName: "m_vest_201",
            id: 165
          },
          {
            clothName: "m_vest_201",
            id: 166
          },
          {
            clothName: "m_vest_201",
            id: 167
          },
          {
            clothName: "m_vest_201",
            id: 168
          },
          {
            clothName: "m_vest_201",
            id: 169
          },
          {
            clothName: "m_vest_201",
            id: 170
          },
          {
            clothName: "m_vest_201",
            id: 171
          },
          {
            clothName: "m_vest_201",
            id: 172
          },
          {
            clothName: "m_vest_201",
            id: 173
          },
          {
            clothName: "m_vest_202",
            id: 174
          },
          {
            clothName: "m_vest_202",
            id: 175
          },
          {
            clothName: "m_vest_202",
            id: 176
          },
          {
            clothName: "m_vest_202",
            id: 177
          },
          {
            clothName: "m_vest_202",
            id: 178
          },
          {
            clothName: "m_vest_202",
            id: 179
          },
          {
            clothName: "m_vest_202",
            id: 180
          },
          {
            clothName: "m_vest_202",
            id: 181
          },
          {
            clothName: "m_vest_202",
            id: 182
          },
          {
            clothName: "m_vest_202",
            id: 183
          },
          {
            clothName: "m_vest_203",
            id: 184
          },
          {
            clothName: "m_vest_203",
            id: 185
          },
          {
            clothName: "m_vest_203",
            id: 186
          },
          {
            clothName: "m_vest_203",
            id: 187
          },
          {
            clothName: "m_vest_203",
            id: 188
          },
          {
            clothName: "m_vest_203",
            id: 189
          },
          {
            clothName: "m_vest_203",
            id: 190
          },
          {
            clothName: "m_vest_203",
            id: 191
          },
          {
            clothName: "m_vest_203",
            id: 192
          },
          {
            clothName: "m_vest_203",
            id: 193
          },
          {
            clothName: "m_vest_204",
            id: 194
          },
          {
            clothName: "m_vest_204",
            id: 195
          },
          {
            clothName: "m_vest_204",
            id: 196
          },
          {
            clothName: "m_vest_204",
            id: 197
          },
          {
            clothName: "m_vest_204",
            id: 198
          },
          {
            clothName: "m_vest_204",
            id: 199
          },
          {
            clothName: "m_vest_204",
            id: 200
          },
          {
            clothName: "m_vest_204",
            id: 201
          },
          {
            clothName: "m_vest_204",
            id: 202
          },
          {
            clothName: "m_vest_204",
            id: 203
          },
          {
            clothName: "m_vest_206",
            id: 204
          },
          {
            clothName: "m_vest_206",
            id: 205
          },
          {
            clothName: "m_vest_206",
            id: 206
          },
          {
            clothName: "m_vest_206",
            id: 207
          },
          {
            clothName: "m_vest_206",
            id: 208
          },
          {
            clothName: "m_vest_206",
            id: 209
          },
          {
            clothName: "m_vest_206",
            id: 210
          },
          {
            clothName: "m_vest_206",
            id: 211
          },
          {
            clothName: "m_vest_206",
            id: 212
          },
          {
            clothName: "m_vest_206",
            id: 213
          }
        ],
        maxValue: 213,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "satchels",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          }
        ],
        maxValue: 1,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "ponchos",
        clothRef: [
          {
            clothName: "m_poncho_000",
            id: 1
          },
          {
            clothName: "m_poncho_000",
            id: 2
          },
          {
            clothName: "m_poncho_000",
            id: 3
          },
          {
            clothName: "m_poncho_000",
            id: 4
          },
          {
            clothName: "m_poncho_000",
            id: 5
          },
          {
            clothName: "m_poncho_000",
            id: 6
          },
          {
            clothName: "m_poncho_000",
            id: 7
          },
          {
            clothName: "m_poncho_000",
            id: 8
          },
          {
            clothName: "m_poncho_000",
            id: 9
          },
          {
            clothName: "m_poncho_000",
            id: 10
          },
          {
            clothName: "m_poncho_001",
            id: 11
          },
          {
            clothName: "m_poncho_001",
            id: 12
          },
          {
            clothName: "m_poncho_001",
            id: 13
          },
          {
            clothName: "m_poncho_001",
            id: 14
          },
          {
            clothName: "m_poncho_001",
            id: 15
          },
          {
            clothName: "m_poncho_001",
            id: 16
          },
          {
            clothName: "m_poncho_001",
            id: 17
          },
          {
            clothName: "m_poncho_001",
            id: 18
          },
          {
            clothName: "m_poncho_001",
            id: 19
          },
          {
            clothName: "m_poncho_001",
            id: 20
          },
          {
            clothName: "m_poncho_002",
            id: 21
          },
          {
            clothName: "m_poncho_002",
            id: 22
          },
          {
            clothName: "m_poncho_002",
            id: 23
          },
          {
            clothName: "m_poncho_002",
            id: 24
          },
          {
            clothName: "m_poncho_002",
            id: 25
          },
          {
            clothName: "m_poncho_002",
            id: 26
          },
          {
            clothName: "m_poncho_002",
            id: 27
          },
          {
            clothName: "m_poncho_002",
            id: 28
          },
          {
            clothName: "m_poncho_002",
            id: 29
          },
          {
            clothName: "m_poncho_002",
            id: 30
          },
          {
            clothName: "m_poncho_003",
            id: 31
          },
          {
            clothName: "m_poncho_003",
            id: 32
          },
          {
            clothName: "m_poncho_003",
            id: 33
          },
          {
            clothName: "m_poncho_003",
            id: 34
          },
          {
            clothName: "m_poncho_003",
            id: 35
          },
          {
            clothName: "m_poncho_003",
            id: 36
          },
          {
            clothName: "m_poncho_003",
            id: 37
          },
          {
            clothName: "m_poncho_003",
            id: 38
          },
          {
            clothName: "m_poncho_003",
            id: 39
          },
          {
            clothName: "m_poncho_003",
            id: 40
          },
          {
            clothName: "m_poncho_004",
            id: 41
          },
          {
            clothName: "m_poncho_004",
            id: 42
          },
          {
            clothName: "m_poncho_004",
            id: 43
          },
          {
            clothName: "m_poncho_004",
            id: 44
          },
          {
            clothName: "m_poncho_004",
            id: 45
          },
          {
            clothName: "m_poncho_004",
            id: 46
          },
          {
            clothName: "m_poncho_004",
            id: 47
          },
          {
            clothName: "m_poncho_004",
            id: 48
          },
          {
            clothName: "m_poncho_004",
            id: 49
          },
          {
            clothName: "m_poncho_004",
            id: 50
          },
          {
            clothName: "m_poncho_005",
            id: 51
          },
          {
            clothName: "m_poncho_005",
            id: 52
          },
          {
            clothName: "m_poncho_005",
            id: 53
          },
          {
            clothName: "m_poncho_005",
            id: 54
          },
          {
            clothName: "m_poncho_005",
            id: 55
          },
          {
            clothName: "m_poncho_005",
            id: 56
          },
          {
            clothName: "m_poncho_005",
            id: 57
          },
          {
            clothName: "m_poncho_005",
            id: 58
          },
          {
            clothName: "m_poncho_005",
            id: 59
          },
          {
            clothName: "m_poncho_005",
            id: 60
          },
          {
            clothName: "m_poncho_006",
            id: 61
          },
          {
            clothName: "m_poncho_006",
            id: 62
          },
          {
            clothName: "m_poncho_006",
            id: 63
          },
          {
            clothName: "m_poncho_006",
            id: 64
          },
          {
            clothName: "m_poncho_006",
            id: 65
          },
          {
            clothName: "m_poncho_006",
            id: 66
          },
          {
            clothName: "m_poncho_006",
            id: 67
          },
          {
            clothName: "m_poncho_006",
            id: 68
          },
          {
            clothName: "m_poncho_006",
            id: 69
          },
          {
            clothName: "m_poncho_006",
            id: 70
          },
          {
            clothName: "unknown_item",
            id: 71
          }
        ],
        maxValue: 71,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "gauntlets",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          },
          {
            clothName: "unknown_item",
            id: 2
          }
        ],
        maxValue: 2,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "boots",
        clothRef: [
          {
            clothName: "boots_000",
            id: 1
          },
          {
            clothName: "boots_000",
            id: 2
          },
          {
            clothName: "boots_000",
            id: 3
          },
          {
            clothName: "boots_000",
            id: 4
          },
          {
            clothName: "boots_000",
            id: 5
          },
          {
            clothName: "boots_000",
            id: 6
          },
          {
            clothName: "boots_000",
            id: 7
          },
          {
            clothName: "boots_000",
            id: 8
          },
          {
            clothName: "boots_000",
            id: 9
          },
          {
            clothName: "boots_000",
            id: 10
          },
          {
            clothName: "boots_001",
            id: 11
          },
          {
            clothName: "boots_001",
            id: 12
          },
          {
            clothName: "boots_001",
            id: 13
          },
          {
            clothName: "boots_001",
            id: 14
          },
          {
            clothName: "boots_001",
            id: 15
          },
          {
            clothName: "boots_001",
            id: 16
          },
          {
            clothName: "boots_001",
            id: 17
          },
          {
            clothName: "boots_001",
            id: 18
          },
          {
            clothName: "boots_001",
            id: 19
          },
          {
            clothName: "boots_001",
            id: 20
          },
          {
            clothName: "boots_002",
            id: 21
          },
          {
            clothName: "boots_002",
            id: 22
          },
          {
            clothName: "boots_002",
            id: 23
          },
          {
            clothName: "boots_002",
            id: 24
          },
          {
            clothName: "boots_002",
            id: 25
          },
          {
            clothName: "boots_002",
            id: 26
          },
          {
            clothName: "boots_002",
            id: 27
          },
          {
            clothName: "boots_002",
            id: 28
          },
          {
            clothName: "boots_002",
            id: 29
          },
          {
            clothName: "boots_002",
            id: 30
          },
          {
            clothName: "m_boots_003",
            id: 31
          },
          {
            clothName: "m_boots_003",
            id: 32
          },
          {
            clothName: "m_boots_003",
            id: 33
          },
          {
            clothName: "m_boots_003",
            id: 34
          },
          {
            clothName: "m_boots_003",
            id: 35
          },
          {
            clothName: "m_boots_003",
            id: 36
          },
          {
            clothName: "m_boots_003",
            id: 37
          },
          {
            clothName: "m_boots_003",
            id: 38
          },
          {
            clothName: "m_boots_003",
            id: 39
          },
          {
            clothName: "m_boots_003",
            id: 40
          },
          {
            clothName: "boots_004",
            id: 41
          },
          {
            clothName: "boots_004",
            id: 42
          },
          {
            clothName: "boots_004",
            id: 43
          },
          {
            clothName: "boots_004",
            id: 44
          },
          {
            clothName: "boots_004",
            id: 45
          },
          {
            clothName: "boots_004",
            id: 46
          },
          {
            clothName: "boots_004",
            id: 47
          },
          {
            clothName: "boots_004",
            id: 48
          },
          {
            clothName: "boots_004",
            id: 49
          },
          {
            clothName: "boots_004",
            id: 50
          },
          {
            clothName: "m_boots_005",
            id: 51
          },
          {
            clothName: "m_boots_005",
            id: 52
          },
          {
            clothName: "m_boots_005",
            id: 53
          },
          {
            clothName: "m_boots_005",
            id: 54
          },
          {
            clothName: "m_boots_005",
            id: 55
          },
          {
            clothName: "m_boots_005",
            id: 56
          },
          {
            clothName: "m_boots_005",
            id: 57
          },
          {
            clothName: "m_boots_005",
            id: 58
          },
          {
            clothName: "m_boots_005",
            id: 59
          },
          {
            clothName: "m_boots_005",
            id: 60
          },
          {
            clothName: "m_boots_006",
            id: 61
          },
          {
            clothName: "m_boots_006",
            id: 62
          },
          {
            clothName: "boots_007",
            id: 63
          },
          {
            clothName: "boots_007",
            id: 64
          },
          {
            clothName: "boots_007",
            id: 65
          },
          {
            clothName: "boots_007",
            id: 66
          },
          {
            clothName: "boots_007",
            id: 67
          },
          {
            clothName: "boots_007",
            id: 68
          },
          {
            clothName: "boots_007",
            id: 69
          },
          {
            clothName: "boots_007",
            id: 70
          },
          {
            clothName: "boots_007",
            id: 71
          },
          {
            clothName: "boots_007",
            id: 72
          },
          {
            clothName: "boots_008",
            id: 73
          },
          {
            clothName: "boots_008",
            id: 74
          },
          {
            clothName: "boots_008",
            id: 75
          },
          {
            clothName: "boots_008",
            id: 76
          },
          {
            clothName: "boots_008",
            id: 77
          },
          {
            clothName: "boots_008",
            id: 78
          },
          {
            clothName: "boots_008",
            id: 79
          },
          {
            clothName: "boots_008",
            id: 80
          },
          {
            clothName: "boots_008",
            id: 81
          },
          {
            clothName: "boots_008",
            id: 82
          },
          {
            clothName: "m_boots_009",
            id: 83
          },
          {
            clothName: "m_boots_009",
            id: 84
          },
          {
            clothName: "m_boots_009",
            id: 85
          },
          {
            clothName: "m_boots_009",
            id: 86
          },
          {
            clothName: "m_boots_009",
            id: 87
          },
          {
            clothName: "m_boots_009",
            id: 88
          },
          {
            clothName: "m_boots_009",
            id: 89
          },
          {
            clothName: "m_boots_009",
            id: 90
          },
          {
            clothName: "m_boots_009",
            id: 91
          },
          {
            clothName: "m_boots_009",
            id: 92
          },
          {
            clothName: "m_boots_010",
            id: 93
          },
          {
            clothName: "m_boots_010",
            id: 94
          },
          {
            clothName: "m_boots_010",
            id: 95
          },
          {
            clothName: "m_boots_010",
            id: 96
          },
          {
            clothName: "m_boots_010",
            id: 97
          },
          {
            clothName: "m_boots_010",
            id: 98
          },
          {
            clothName: "m_boots_010",
            id: 99
          },
          {
            clothName: "m_boots_010",
            id: 100
          },
          {
            clothName: "m_boots_010",
            id: 101
          },
          {
            clothName: "m_boots_010",
            id: 102
          },
          {
            clothName: "boots_011",
            id: 103
          },
          {
            clothName: "boots_011",
            id: 104
          },
          {
            clothName: "boots_011",
            id: 105
          },
          {
            clothName: "boots_011",
            id: 106
          },
          {
            clothName: "boots_011",
            id: 107
          },
          {
            clothName: "boots_011",
            id: 108
          },
          {
            clothName: "boots_011",
            id: 109
          },
          {
            clothName: "boots_011",
            id: 110
          },
          {
            clothName: "boots_011",
            id: 111
          },
          {
            clothName: "boots_011",
            id: 112
          },
          {
            clothName: "boots_012",
            id: 113
          },
          {
            clothName: "boots_012",
            id: 114
          },
          {
            clothName: "boots_012",
            id: 115
          },
          {
            clothName: "boots_012",
            id: 116
          },
          {
            clothName: "boots_012",
            id: 117
          },
          {
            clothName: "boots_012",
            id: 118
          },
          {
            clothName: "boots_012",
            id: 119
          },
          {
            clothName: "boots_012",
            id: 120
          },
          {
            clothName: "boots_012",
            id: 121
          },
          {
            clothName: "boots_012",
            id: 122
          },
          {
            clothName: "boots_013",
            id: 123
          },
          {
            clothName: "boots_013",
            id: 124
          },
          {
            clothName: "boots_013",
            id: 125
          },
          {
            clothName: "boots_013",
            id: 126
          },
          {
            clothName: "boots_013",
            id: 127
          },
          {
            clothName: "boots_013",
            id: 128
          },
          {
            clothName: "boots_013",
            id: 129
          },
          {
            clothName: "boots_013",
            id: 130
          },
          {
            clothName: "boots_013",
            id: 131
          },
          {
            clothName: "boots_013",
            id: 132
          },
          {
            clothName: "boots_014",
            id: 133
          },
          {
            clothName: "boots_014",
            id: 134
          },
          {
            clothName: "boots_014",
            id: 135
          },
          {
            clothName: "boots_014",
            id: 136
          },
          {
            clothName: "boots_014",
            id: 137
          },
          {
            clothName: "boots_014",
            id: 138
          },
          {
            clothName: "boots_014",
            id: 139
          },
          {
            clothName: "boots_014",
            id: 140
          },
          {
            clothName: "boots_014",
            id: 141
          },
          {
            clothName: "boots_014",
            id: 142
          },
          {
            clothName: "m_boots_015",
            id: 143
          },
          {
            clothName: "m_boots_015",
            id: 144
          },
          {
            clothName: "m_boots_015",
            id: 145
          },
          {
            clothName: "m_boots_015",
            id: 146
          },
          {
            clothName: "m_boots_015",
            id: 147
          },
          {
            clothName: "m_boots_015",
            id: 148
          },
          {
            clothName: "m_boots_015",
            id: 149
          },
          {
            clothName: "m_boots_015",
            id: 150
          },
          {
            clothName: "m_boots_015",
            id: 151
          },
          {
            clothName: "m_boots_015",
            id: 152
          },
          {
            clothName: "m_boots_017",
            id: 153
          },
          {
            clothName: "m_boots_017",
            id: 154
          },
          {
            clothName: "m_boots_017",
            id: 155
          },
          {
            clothName: "m_boots_017",
            id: 156
          },
          {
            clothName: "m_boots_017",
            id: 157
          },
          {
            clothName: "m_boots_017",
            id: 158
          },
          {
            clothName: "m_boots_017",
            id: 159
          },
          {
            clothName: "m_boots_017",
            id: 160
          },
          {
            clothName: "m_boots_017",
            id: 161
          },
          {
            clothName: "m_boots_017",
            id: 162
          },
          {
            clothName: "m_boots_021",
            id: 163
          },
          {
            clothName: "m_boots_021",
            id: 164
          },
          {
            clothName: "m_boots_021",
            id: 165
          },
          {
            clothName: "m_boots_021",
            id: 166
          },
          {
            clothName: "m_boots_021",
            id: 167
          },
          {
            clothName: "m_boots_021",
            id: 168
          },
          {
            clothName: "m_boots_021",
            id: 169
          },
          {
            clothName: "m_boots_021",
            id: 170
          },
          {
            clothName: "m_boots_021",
            id: 171
          },
          {
            clothName: "m_boots_021",
            id: 172
          },
          {
            clothName: "m_boots_021",
            id: 173
          },
          {
            clothName: "m_boots_021",
            id: 174
          },
          {
            clothName: "m_boots_021",
            id: 175
          },
          {
            clothName: "m_boots_021",
            id: 176
          },
          {
            clothName: "m_boots_022",
            id: 177
          },
          {
            clothName: "m_boots_022",
            id: 178
          },
          {
            clothName: "m_boots_022",
            id: 179
          },
          {
            clothName: "m_boots_022",
            id: 180
          },
          {
            clothName: "m_boots_022",
            id: 181
          },
          {
            clothName: "m_boots_022",
            id: 182
          },
          {
            clothName: "m_boots_022",
            id: 183
          },
          {
            clothName: "m_boots_022",
            id: 184
          },
          {
            clothName: "m_boots_022",
            id: 185
          },
          {
            clothName: "m_boots_022",
            id: 186
          },
          {
            clothName: "m_boots_023",
            id: 187
          },
          {
            clothName: "m_boots_023",
            id: 188
          },
          {
            clothName: "m_boots_023",
            id: 189
          },
          {
            clothName: "m_boots_023",
            id: 190
          },
          {
            clothName: "m_boots_023",
            id: 191
          },
          {
            clothName: "m_boots_023",
            id: 192
          },
          {
            clothName: "m_boots_023",
            id: 193
          },
          {
            clothName: "m_boots_023",
            id: 194
          },
          {
            clothName: "m_boots_023",
            id: 195
          },
          {
            clothName: "m_boots_023",
            id: 196
          },
          {
            clothName: "m_boots_027",
            id: 197
          },
          {
            clothName: "m_boots_027",
            id: 198
          },
          {
            clothName: "m_boots_027",
            id: 199
          },
          {
            clothName: "m_boots_027",
            id: 200
          },
          {
            clothName: "m_boots_027",
            id: 201
          },
          {
            clothName: "m_boots_027",
            id: 202
          },
          {
            clothName: "m_boots_027",
            id: 203
          },
          {
            clothName: "m_boots_027",
            id: 204
          },
          {
            clothName: "m_boots_027",
            id: 205
          },
          {
            clothName: "m_boots_027",
            id: 206
          },
          {
            clothName: "m_boots_028",
            id: 207
          },
          {
            clothName: "m_boots_028",
            id: 208
          },
          {
            clothName: "m_boots_028",
            id: 209
          },
          {
            clothName: "m_boots_028",
            id: 210
          },
          {
            clothName: "m_boots_028",
            id: 211
          },
          {
            clothName: "m_boots_028",
            id: 212
          },
          {
            clothName: "m_boots_028",
            id: 213
          },
          {
            clothName: "m_boots_028",
            id: 214
          },
          {
            clothName: "m_boots_028",
            id: 215
          },
          {
            clothName: "m_boots_028",
            id: 216
          },
          {
            clothName: "m_boots_029",
            id: 217
          },
          {
            clothName: "m_boots_029",
            id: 218
          },
          {
            clothName: "m_boots_029",
            id: 219
          },
          {
            clothName: "m_boots_029",
            id: 220
          },
          {
            clothName: "m_boots_029",
            id: 221
          },
          {
            clothName: "m_boots_029",
            id: 222
          },
          {
            clothName: "m_boots_029",
            id: 223
          },
          {
            clothName: "m_boots_029",
            id: 224
          },
          {
            clothName: "m_boots_029",
            id: 225
          },
          {
            clothName: "m_boots_029",
            id: 226
          },
          {
            clothName: "m_boots_030",
            id: 227
          },
          {
            clothName: "m_boots_030",
            id: 228
          },
          {
            clothName: "m_boots_030",
            id: 229
          },
          {
            clothName: "m_boots_030",
            id: 230
          },
          {
            clothName: "m_boots_030",
            id: 231
          },
          {
            clothName: "m_boots_030",
            id: 232
          },
          {
            clothName: "m_boots_030",
            id: 233
          },
          {
            clothName: "m_boots_030",
            id: 234
          },
          {
            clothName: "m_boots_030",
            id: 235
          },
          {
            clothName: "m_boots_030",
            id: 236
          },
          {
            clothName: "m_boots_200",
            id: 237
          },
          {
            clothName: "m_boots_200",
            id: 238
          },
          {
            clothName: "m_boots_200",
            id: 239
          },
          {
            clothName: "m_boots_200",
            id: 240
          },
          {
            clothName: "m_boots_200",
            id: 241
          },
          {
            clothName: "m_boots_200",
            id: 242
          },
          {
            clothName: "m_boots_200",
            id: 243
          },
          {
            clothName: "m_boots_200",
            id: 244
          },
          {
            clothName: "m_boots_200",
            id: 245
          },
          {
            clothName: "m_boots_200",
            id: 246
          },
          {
            clothName: "m_boots_201",
            id: 247
          },
          {
            clothName: "m_boots_201",
            id: 248
          },
          {
            clothName: "m_boots_201",
            id: 249
          },
          {
            clothName: "m_boots_201",
            id: 250
          },
          {
            clothName: "m_boots_201",
            id: 251
          },
          {
            clothName: "m_boots_201",
            id: 252
          },
          {
            clothName: "m_boots_201",
            id: 253
          },
          {
            clothName: "m_boots_201",
            id: 254
          },
          {
            clothName: "m_boots_201",
            id: 255
          },
          {
            clothName: "m_boots_201",
            id: 256
          },
          {
            clothName: "m_boots_202",
            id: 257
          },
          {
            clothName: "m_boots_202",
            id: 258
          },
          {
            clothName: "m_boots_202",
            id: 259
          },
          {
            clothName: "m_boots_202",
            id: 260
          },
          {
            clothName: "m_boots_202",
            id: 261
          },
          {
            clothName: "m_boots_202",
            id: 262
          },
          {
            clothName: "m_boots_202",
            id: 263
          },
          {
            clothName: "m_boots_202",
            id: 264
          },
          {
            clothName: "m_boots_202",
            id: 265
          },
          {
            clothName: "m_boots_202",
            id: 266
          },
          {
            clothName: "m_boots_203",
            id: 267
          },
          {
            clothName: "m_boots_203",
            id: 268
          },
          {
            clothName: "m_boots_203",
            id: 269
          },
          {
            clothName: "m_boots_203",
            id: 270
          },
          {
            clothName: "m_boots_203",
            id: 271
          },
          {
            clothName: "m_boots_203",
            id: 272
          },
          {
            clothName: "m_boots_203",
            id: 273
          },
          {
            clothName: "m_boots_203",
            id: 274
          },
          {
            clothName: "m_boots_203",
            id: 275
          },
          {
            clothName: "m_boots_203",
            id: 276
          },
          {
            clothName: "m_boots_204",
            id: 277
          },
          {
            clothName: "m_boots_204",
            id: 278
          },
          {
            clothName: "m_boots_204",
            id: 279
          },
          {
            clothName: "m_boots_204",
            id: 280
          },
          {
            clothName: "m_boots_204",
            id: 281
          },
          {
            clothName: "m_boots_204",
            id: 282
          },
          {
            clothName: "m_boots_204",
            id: 283
          },
          {
            clothName: "m_boots_204",
            id: 284
          },
          {
            clothName: "m_boots_204",
            id: 285
          },
          {
            clothName: "m_boots_204",
            id: 286
          },
          {
            clothName: "m_boots_205",
            id: 287
          },
          {
            clothName: "m_boots_205",
            id: 288
          },
          {
            clothName: "m_boots_205",
            id: 289
          },
          {
            clothName: "m_boots_205",
            id: 290
          },
          {
            clothName: "m_boots_205",
            id: 291
          },
          {
            clothName: "m_boots_205",
            id: 292
          },
          {
            clothName: "m_boots_205",
            id: 293
          },
          {
            clothName: "m_boots_205",
            id: 294
          },
          {
            clothName: "m_boots_205",
            id: 295
          },
          {
            clothName: "m_boots_205",
            id: 296
          },
          {
            clothName: "m_boots_206",
            id: 297
          },
          {
            clothName: "m_boots_206",
            id: 298
          },
          {
            clothName: "m_boots_206",
            id: 299
          },
          {
            clothName: "m_boots_206",
            id: 300
          },
          {
            clothName: "m_boots_206",
            id: 301
          },
          {
            clothName: "m_boots_206",
            id: 302
          },
          {
            clothName: "m_boots_206",
            id: 303
          },
          {
            clothName: "m_boots_206",
            id: 304
          },
          {
            clothName: "m_boots_206",
            id: 305
          },
          {
            clothName: "m_boots_206",
            id: 306
          },
          {
            clothName: "m_boots_207",
            id: 307
          },
          {
            clothName: "m_boots_207",
            id: 308
          },
          {
            clothName: "m_boots_207",
            id: 309
          },
          {
            clothName: "m_boots_207",
            id: 310
          },
          {
            clothName: "m_boots_207",
            id: 311
          },
          {
            clothName: "m_boots_207",
            id: 312
          },
          {
            clothName: "m_boots_207",
            id: 313
          },
          {
            clothName: "m_boots_207",
            id: 314
          },
          {
            clothName: "m_boots_207",
            id: 315
          },
          {
            clothName: "m_boots_207",
            id: 316
          },
          {
            clothName: "m_boots_208",
            id: 317
          },
          {
            clothName: "m_boots_208",
            id: 318
          },
          {
            clothName: "m_boots_208",
            id: 319
          },
          {
            clothName: "m_boots_208",
            id: 320
          },
          {
            clothName: "m_boots_208",
            id: 321
          },
          {
            clothName: "m_boots_208",
            id: 322
          },
          {
            clothName: "m_boots_208",
            id: 323
          },
          {
            clothName: "m_boots_208",
            id: 324
          },
          {
            clothName: "m_boots_208",
            id: 325
          },
          {
            clothName: "m_boots_208",
            id: 326
          },
          {
            clothName: "m_boots_211",
            id: 327
          },
          {
            clothName: "m_boots_211",
            id: 328
          },
          {
            clothName: "m_boots_211",
            id: 329
          },
          {
            clothName: "m_boots_211",
            id: 330
          },
          {
            clothName: "m_boots_211",
            id: 331
          },
          {
            clothName: "m_boots_211",
            id: 332
          },
          {
            clothName: "m_boots_211",
            id: 333
          },
          {
            clothName: "m_boots_211",
            id: 334
          },
          {
            clothName: "m_boots_211",
            id: 335
          },
          {
            clothName: "m_boots_211",
            id: 336
          },
          {
            clothName: "m_boots_212",
            id: 337
          },
          {
            clothName: "m_boots_212",
            id: 338
          },
          {
            clothName: "m_boots_212",
            id: 339
          },
          {
            clothName: "m_boots_212",
            id: 340
          },
          {
            clothName: "m_boots_212",
            id: 341
          },
          {
            clothName: "m_boots_212",
            id: 342
          },
          {
            clothName: "m_boots_212",
            id: 343
          },
          {
            clothName: "m_boots_212",
            id: 344
          },
          {
            clothName: "m_boots_212",
            id: 345
          },
          {
            clothName: "m_boots_212",
            id: 346
          },
          {
            clothName: "m_boots_213",
            id: 347
          },
          {
            clothName: "m_boots_213",
            id: 348
          },
          {
            clothName: "m_boots_213",
            id: 349
          },
          {
            clothName: "m_boots_213",
            id: 350
          },
          {
            clothName: "m_boots_213",
            id: 351
          },
          {
            clothName: "m_boots_213",
            id: 352
          },
          {
            clothName: "m_boots_213",
            id: 353
          },
          {
            clothName: "m_boots_213",
            id: 354
          },
          {
            clothName: "m_boots_213",
            id: 355
          },
          {
            clothName: "m_boots_213",
            id: 356
          },
          {
            clothName: "m_boots_214",
            id: 357
          },
          {
            clothName: "m_boots_214",
            id: 358
          },
          {
            clothName: "m_boots_214",
            id: 359
          },
          {
            clothName: "m_boots_214",
            id: 360
          },
          {
            clothName: "m_boots_214",
            id: 361
          },
          {
            clothName: "m_boots_214",
            id: 362
          },
          {
            clothName: "m_boots_214",
            id: 363
          },
          {
            clothName: "m_boots_214",
            id: 364
          },
          {
            clothName: "m_boots_214",
            id: 365
          },
          {
            clothName: "m_boots_214",
            id: 366
          },
          {
            clothName: "m_boots_215",
            id: 367
          },
          {
            clothName: "m_boots_215",
            id: 368
          },
          {
            clothName: "m_boots_215",
            id: 369
          },
          {
            clothName: "m_boots_215",
            id: 370
          },
          {
            clothName: "m_boots_215",
            id: 371
          },
          {
            clothName: "m_boots_215",
            id: 372
          },
          {
            clothName: "m_boots_215",
            id: 373
          },
          {
            clothName: "m_boots_215",
            id: 374
          },
          {
            clothName: "m_boots_215",
            id: 375
          },
          {
            clothName: "m_boots_215",
            id: 376
          },
          {
            clothName: "m_boots_216",
            id: 377
          },
          {
            clothName: "m_boots_216",
            id: 378
          },
          {
            clothName: "m_boots_216",
            id: 379
          },
          {
            clothName: "m_boots_216",
            id: 380
          },
          {
            clothName: "m_boots_216",
            id: 381
          },
          {
            clothName: "m_boots_216",
            id: 382
          },
          {
            clothName: "m_boots_216",
            id: 383
          },
          {
            clothName: "m_boots_216",
            id: 384
          },
          {
            clothName: "m_boots_216",
            id: 385
          },
          {
            clothName: "m_boots_216",
            id: 386
          },
          {
            clothName: "m_boots_217",
            id: 387
          },
          {
            clothName: "m_boots_217",
            id: 388
          },
          {
            clothName: "m_boots_217",
            id: 389
          },
          {
            clothName: "m_boots_217",
            id: 390
          },
          {
            clothName: "m_boots_217",
            id: 391
          },
          {
            clothName: "m_boots_217",
            id: 392
          },
          {
            clothName: "m_boots_217",
            id: 393
          },
          {
            clothName: "m_boots_217",
            id: 394
          },
          {
            clothName: "m_boots_217",
            id: 395
          },
          {
            clothName: "m_boots_217",
            id: 396
          },
          {
            clothName: "m_boots_218",
            id: 397
          },
          {
            clothName: "m_boots_218",
            id: 398
          },
          {
            clothName: "m_boots_218",
            id: 399
          },
          {
            clothName: "m_boots_218",
            id: 400
          },
          {
            clothName: "m_boots_218",
            id: 401
          },
          {
            clothName: "m_boots_218",
            id: 402
          },
          {
            clothName: "m_boots_218",
            id: 403
          },
          {
            clothName: "m_boots_218",
            id: 404
          },
          {
            clothName: "m_boots_218",
            id: 405
          },
          {
            clothName: "m_boots_218",
            id: 406
          },
          {
            clothName: "unknown_item",
            id: 407
          },
          {
            clothName: "boots_000",
            id: 408
          },
          {
            clothName: "boots_000",
            id: 409
          },
          {
            clothName: "unknown_item",
            id: 410
          },
          {
            clothName: "boots_000",
            id: 411
          },
          {
            clothName: "unknown_item",
            id: 412
          },
          {
            clothName: "unknown_item",
            id: 413
          },
          {
            clothName: "boots_000",
            id: 414
          }
        ],
        maxValue: 414,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "pants",
        clothRef: [
          {
            clothName: "pants_000",
            id: 1
          },
          {
            clothName: "pants_000",
            id: 2
          },
          {
            clothName: "overalls_001",
            id: 3
          },
          {
            clothName: "overalls_001",
            id: 4
          },
          {
            clothName: "overalls_001",
            id: 5
          },
          {
            clothName: "overalls_001",
            id: 6
          },
          {
            clothName: "overalls_001",
            id: 7
          },
          {
            clothName: "overalls_001",
            id: 8
          },
          {
            clothName: "overalls_001",
            id: 9
          },
          {
            clothName: "overalls_001",
            id: 10
          },
          {
            clothName: "overalls_002",
            id: 11
          },
          {
            clothName: "overalls_002",
            id: 12
          },
          {
            clothName: "overalls_002",
            id: 13
          },
          {
            clothName: "overalls_002",
            id: 14
          },
          {
            clothName: "overalls_002",
            id: 15
          },
          {
            clothName: "overalls_002",
            id: 16
          },
          {
            clothName: "overalls_002",
            id: 17
          },
          {
            clothName: "overalls_002",
            id: 18
          },
          {
            clothName: "pants_000",
            id: 19
          },
          {
            clothName: "pants_000",
            id: 20
          },
          {
            clothName: "pants_000",
            id: 21
          },
          {
            clothName: "pants_000",
            id: 22
          },
          {
            clothName: "pants_000",
            id: 23
          },
          {
            clothName: "pants_000",
            id: 24
          },
          {
            clothName: "pants_000",
            id: 25
          },
          {
            clothName: "pants_000",
            id: 26
          },
          {
            clothName: "pants_000",
            id: 27
          },
          {
            clothName: "pants_000",
            id: 28
          },
          {
            clothName: "pants_000",
            id: 29
          },
          {
            clothName: "pants_000",
            id: 30
          },
          {
            clothName: "pants_000",
            id: 31
          },
          {
            clothName: "pants_000",
            id: 32
          },
          {
            clothName: "pants_000",
            id: 33
          },
          {
            clothName: "pants_000",
            id: 34
          },
          {
            clothName: "pants_000",
            id: 35
          },
          {
            clothName: "pants_000",
            id: 36
          },
          {
            clothName: "pants_000",
            id: 37
          },
          {
            clothName: "pants_000",
            id: 38
          },
          {
            clothName: "pants_001",
            id: 39
          },
          {
            clothName: "pants_001",
            id: 40
          },
          {
            clothName: "pants_001",
            id: 41
          },
          {
            clothName: "pants_001",
            id: 42
          },
          {
            clothName: "pants_001",
            id: 43
          },
          {
            clothName: "pants_001",
            id: 44
          },
          {
            clothName: "pants_001",
            id: 45
          },
          {
            clothName: "pants_001",
            id: 46
          },
          {
            clothName: "pants_001",
            id: 47
          },
          {
            clothName: "pants_001",
            id: 48
          },
          {
            clothName: "pants_002",
            id: 49
          },
          {
            clothName: "pants_002",
            id: 50
          },
          {
            clothName: "pants_002",
            id: 51
          },
          {
            clothName: "pants_002",
            id: 52
          },
          {
            clothName: "pants_002",
            id: 53
          },
          {
            clothName: "pants_002",
            id: 54
          },
          {
            clothName: "pants_002",
            id: 55
          },
          {
            clothName: "pants_002",
            id: 56
          },
          {
            clothName: "pants_002",
            id: 57
          },
          {
            clothName: "pants_002",
            id: 58
          },
          {
            clothName: "m_pants_003",
            id: 59
          },
          {
            clothName: "m_pants_003",
            id: 60
          },
          {
            clothName: "m_pants_003",
            id: 61
          },
          {
            clothName: "m_pants_003",
            id: 62
          },
          {
            clothName: "m_pants_003",
            id: 63
          },
          {
            clothName: "m_pants_003",
            id: 64
          },
          {
            clothName: "m_pants_003",
            id: 65
          },
          {
            clothName: "m_pants_003",
            id: 66
          },
          {
            clothName: "m_pants_003",
            id: 67
          },
          {
            clothName: "m_pants_003",
            id: 68
          },
          {
            clothName: "m_pants_004",
            id: 69
          },
          {
            clothName: "m_pants_004",
            id: 70
          },
          {
            clothName: "m_pants_004",
            id: 71
          },
          {
            clothName: "m_pants_004",
            id: 72
          },
          {
            clothName: "m_pants_004",
            id: 73
          },
          {
            clothName: "m_pants_004",
            id: 74
          },
          {
            clothName: "m_pants_004",
            id: 75
          },
          {
            clothName: "m_pants_004",
            id: 76
          },
          {
            clothName: "m_pants_004",
            id: 77
          },
          {
            clothName: "m_pants_004",
            id: 78
          },
          {
            clothName: "pants_005",
            id: 79
          },
          {
            clothName: "pants_005",
            id: 80
          },
          {
            clothName: "pants_005",
            id: 81
          },
          {
            clothName: "pants_005",
            id: 82
          },
          {
            clothName: "pants_005",
            id: 83
          },
          {
            clothName: "pants_005",
            id: 84
          },
          {
            clothName: "pants_005",
            id: 85
          },
          {
            clothName: "pants_005",
            id: 86
          },
          {
            clothName: "pants_005",
            id: 87
          },
          {
            clothName: "pants_005",
            id: 88
          },
          {
            clothName: "pants_006",
            id: 89
          },
          {
            clothName: "pants_006",
            id: 90
          },
          {
            clothName: "pants_006",
            id: 91
          },
          {
            clothName: "pants_006",
            id: 92
          },
          {
            clothName: "pants_006",
            id: 93
          },
          {
            clothName: "pants_006",
            id: 94
          },
          {
            clothName: "pants_006",
            id: 95
          },
          {
            clothName: "pants_006",
            id: 96
          },
          {
            clothName: "pants_006",
            id: 97
          },
          {
            clothName: "pants_006",
            id: 98
          },
          {
            clothName: "pants_007",
            id: 99
          },
          {
            clothName: "pants_007",
            id: 100
          },
          {
            clothName: "pants_007",
            id: 101
          },
          {
            clothName: "pants_007",
            id: 102
          },
          {
            clothName: "pants_007",
            id: 103
          },
          {
            clothName: "pants_007",
            id: 104
          },
          {
            clothName: "pants_007",
            id: 105
          },
          {
            clothName: "pants_007",
            id: 106
          },
          {
            clothName: "pants_007",
            id: 107
          },
          {
            clothName: "pants_007",
            id: 108
          },
          {
            clothName: "pants_008",
            id: 109
          },
          {
            clothName: "pants_008",
            id: 110
          },
          {
            clothName: "pants_008",
            id: 111
          },
          {
            clothName: "pants_008",
            id: 112
          },
          {
            clothName: "pants_008",
            id: 113
          },
          {
            clothName: "pants_008",
            id: 114
          },
          {
            clothName: "pants_008",
            id: 115
          },
          {
            clothName: "pants_008",
            id: 116
          },
          {
            clothName: "pants_008",
            id: 117
          },
          {
            clothName: "pants_008",
            id: 118
          },
          {
            clothName: "pants_008",
            id: 119
          },
          {
            clothName: "pants_008",
            id: 120
          },
          {
            clothName: "pants_008",
            id: 121
          },
          {
            clothName: "pants_008",
            id: 122
          },
          {
            clothName: "pants_008",
            id: 123
          },
          {
            clothName: "pants_008",
            id: 124
          },
          {
            clothName: "pants_008",
            id: 125
          },
          {
            clothName: "pants_008",
            id: 126
          },
          {
            clothName: "pants_008",
            id: 127
          },
          {
            clothName: "pants_008",
            id: 128
          },
          {
            clothName: "m_pants_009",
            id: 129
          },
          {
            clothName: "m_pants_009",
            id: 130
          },
          {
            clothName: "m_pants_009",
            id: 131
          },
          {
            clothName: "m_pants_009",
            id: 132
          },
          {
            clothName: "m_pants_009",
            id: 133
          },
          {
            clothName: "m_pants_009",
            id: 134
          },
          {
            clothName: "m_pants_009",
            id: 135
          },
          {
            clothName: "m_pants_009",
            id: 136
          },
          {
            clothName: "m_pants_009",
            id: 137
          },
          {
            clothName: "m_pants_009",
            id: 138
          },
          {
            clothName: "pants_013",
            id: 139
          },
          {
            clothName: "pants_013",
            id: 140
          },
          {
            clothName: "pants_013",
            id: 141
          },
          {
            clothName: "pants_013",
            id: 142
          },
          {
            clothName: "pants_013",
            id: 143
          },
          {
            clothName: "pants_013",
            id: 144
          },
          {
            clothName: "pants_013",
            id: 145
          },
          {
            clothName: "pants_013",
            id: 146
          },
          {
            clothName: "pants_013",
            id: 147
          },
          {
            clothName: "pants_013",
            id: 148
          },
          {
            clothName: "pants_013",
            id: 149
          },
          {
            clothName: "m_pants_014",
            id: 150
          },
          {
            clothName: "m_pants_014",
            id: 151
          },
          {
            clothName: "m_pants_014",
            id: 152
          },
          {
            clothName: "m_pants_014",
            id: 153
          },
          {
            clothName: "m_pants_014",
            id: 154
          },
          {
            clothName: "m_pants_014",
            id: 155
          },
          {
            clothName: "m_pants_014",
            id: 156
          },
          {
            clothName: "m_pants_014",
            id: 157
          },
          {
            clothName: "m_pants_014",
            id: 158
          },
          {
            clothName: "m_pants_014",
            id: 159
          },
          {
            clothName: "m_pants_015",
            id: 160
          },
          {
            clothName: "m_pants_015",
            id: 161
          },
          {
            clothName: "m_pants_015",
            id: 162
          },
          {
            clothName: "m_pants_015",
            id: 163
          },
          {
            clothName: "m_pants_015",
            id: 164
          },
          {
            clothName: "m_pants_015",
            id: 165
          },
          {
            clothName: "m_pants_015",
            id: 166
          },
          {
            clothName: "m_pants_015",
            id: 167
          },
          {
            clothName: "m_pants_015",
            id: 168
          },
          {
            clothName: "m_pants_015",
            id: 169
          },
          {
            clothName: "m_pants_016",
            id: 170
          },
          {
            clothName: "m_pants_016",
            id: 171
          },
          {
            clothName: "m_pants_016",
            id: 172
          },
          {
            clothName: "m_pants_016",
            id: 173
          },
          {
            clothName: "m_pants_016",
            id: 174
          },
          {
            clothName: "m_pants_016",
            id: 175
          },
          {
            clothName: "m_pants_016",
            id: 176
          },
          {
            clothName: "m_pants_016",
            id: 177
          },
          {
            clothName: "m_pants_016",
            id: 178
          },
          {
            clothName: "m_pants_016",
            id: 179
          },
          {
            clothName: "m_pants_018",
            id: 180
          },
          {
            clothName: "m_pants_018",
            id: 181
          },
          {
            clothName: "m_pants_018",
            id: 182
          },
          {
            clothName: "m_pants_018",
            id: 183
          },
          {
            clothName: "m_pants_018",
            id: 184
          },
          {
            clothName: "m_pants_018",
            id: 185
          },
          {
            clothName: "m_pants_018",
            id: 186
          },
          {
            clothName: "m_pants_018",
            id: 187
          },
          {
            clothName: "m_pants_018",
            id: 188
          },
          {
            clothName: "m_pants_018",
            id: 189
          },
          {
            clothName: "m_pants_019",
            id: 190
          },
          {
            clothName: "m_pants_019",
            id: 191
          },
          {
            clothName: "m_pants_019",
            id: 192
          },
          {
            clothName: "m_pants_019",
            id: 193
          },
          {
            clothName: "m_pants_019",
            id: 194
          },
          {
            clothName: "m_pants_019",
            id: 195
          },
          {
            clothName: "m_pants_019",
            id: 196
          },
          {
            clothName: "m_pants_019",
            id: 197
          },
          {
            clothName: "m_pants_019",
            id: 198
          },
          {
            clothName: "m_pants_019",
            id: 199
          },
          {
            clothName: "m_pants_020",
            id: 200
          },
          {
            clothName: "m_pants_020",
            id: 201
          },
          {
            clothName: "m_pants_020",
            id: 202
          },
          {
            clothName: "m_pants_020",
            id: 203
          },
          {
            clothName: "m_pants_020",
            id: 204
          },
          {
            clothName: "m_pants_020",
            id: 205
          },
          {
            clothName: "m_pants_020",
            id: 206
          },
          {
            clothName: "m_pants_020",
            id: 207
          },
          {
            clothName: "m_pants_020",
            id: 208
          },
          {
            clothName: "m_pants_020",
            id: 209
          },
          {
            clothName: "m_pants_200",
            id: 210
          },
          {
            clothName: "m_pants_200",
            id: 211
          },
          {
            clothName: "m_pants_200",
            id: 212
          },
          {
            clothName: "m_pants_200",
            id: 213
          },
          {
            clothName: "m_pants_200",
            id: 214
          },
          {
            clothName: "m_pants_200",
            id: 215
          },
          {
            clothName: "m_pants_200",
            id: 216
          },
          {
            clothName: "m_pants_200",
            id: 217
          },
          {
            clothName: "m_pants_200",
            id: 218
          },
          {
            clothName: "m_pants_200",
            id: 219
          },
          {
            clothName: "m_pants_201",
            id: 220
          },
          {
            clothName: "m_pants_201",
            id: 221
          },
          {
            clothName: "m_pants_201",
            id: 222
          },
          {
            clothName: "m_pants_201",
            id: 223
          },
          {
            clothName: "m_pants_201",
            id: 224
          },
          {
            clothName: "m_pants_201",
            id: 225
          },
          {
            clothName: "m_pants_201",
            id: 226
          },
          {
            clothName: "m_pants_201",
            id: 227
          },
          {
            clothName: "m_pants_201",
            id: 228
          },
          {
            clothName: "m_pants_201",
            id: 229
          },
          {
            clothName: "m_pants_202",
            id: 230
          },
          {
            clothName: "m_pants_202",
            id: 231
          },
          {
            clothName: "m_pants_202",
            id: 232
          },
          {
            clothName: "m_pants_202",
            id: 233
          },
          {
            clothName: "m_pants_202",
            id: 234
          },
          {
            clothName: "m_pants_202",
            id: 235
          },
          {
            clothName: "m_pants_202",
            id: 236
          },
          {
            clothName: "m_pants_202",
            id: 237
          },
          {
            clothName: "m_pants_202",
            id: 238
          },
          {
            clothName: "m_pants_202",
            id: 239
          },
          {
            clothName: "m_pants_203",
            id: 240
          },
          {
            clothName: "m_pants_203",
            id: 241
          },
          {
            clothName: "m_pants_203",
            id: 242
          },
          {
            clothName: "m_pants_203",
            id: 243
          },
          {
            clothName: "m_pants_203",
            id: 244
          },
          {
            clothName: "m_pants_203",
            id: 245
          },
          {
            clothName: "m_pants_203",
            id: 246
          },
          {
            clothName: "m_pants_203",
            id: 247
          },
          {
            clothName: "m_pants_203",
            id: 248
          },
          {
            clothName: "m_pants_203",
            id: 249
          },
          {
            clothName: "m_pants_204",
            id: 250
          },
          {
            clothName: "m_pants_204",
            id: 251
          },
          {
            clothName: "m_pants_204",
            id: 252
          },
          {
            clothName: "m_pants_204",
            id: 253
          },
          {
            clothName: "m_pants_204",
            id: 254
          },
          {
            clothName: "m_pants_204",
            id: 255
          },
          {
            clothName: "m_pants_204",
            id: 256
          },
          {
            clothName: "m_pants_204",
            id: 257
          },
          {
            clothName: "m_pants_205",
            id: 258
          },
          {
            clothName: "m_pants_205",
            id: 259
          },
          {
            clothName: "m_pants_205",
            id: 260
          },
          {
            clothName: "m_pants_205",
            id: 261
          },
          {
            clothName: "m_pants_205",
            id: 262
          },
          {
            clothName: "m_pants_205",
            id: 263
          },
          {
            clothName: "m_pants_205",
            id: 264
          },
          {
            clothName: "m_pants_205",
            id: 265
          },
          {
            clothName: "unknown_item",
            id: 266
          },
          {
            clothName: "unknown_item",
            id: 267
          },
          {
            clothName: "pants_000",
            id: 268
          },
          {
            clothName: "unknown_item",
            id: 269
          },
          {
            clothName: "unknown_item",
            id: 270
          },
          {
            clothName: "unknown_item",
            id: 271
          },
          {
            clothName: "unknown_item",
            id: 272
          },
          {
            clothName: "unknown_item",
            id: 273
          },
          {
            clothName: "unknown_item",
            id: 274
          },
          {
            clothName: "unknown_item",
            id: 275
          },
          {
            clothName: "unknown_item",
            id: 276
          },
          {
            clothName: "unknown_item",
            id: 277
          },
          {
            clothName: "pants_000",
            id: 278
          }
        ],
        maxValue: 278,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "hats",
        clothRef: [
          {
            clothName: "hat_000",
            id: 1
          },
          {
            clothName: "hat_000",
            id: 2
          },
          {
            clothName: "hat_000",
            id: 3
          },
          {
            clothName: "hat_000",
            id: 4
          },
          {
            clothName: "hat_000",
            id: 5
          },
          {
            clothName: "hat_000",
            id: 6
          },
          {
            clothName: "hat_000",
            id: 7
          },
          {
            clothName: "hat_000",
            id: 8
          },
          {
            clothName: "hat_000",
            id: 9
          },
          {
            clothName: "hat_001",
            id: 10
          },
          {
            clothName: "hat_001",
            id: 11
          },
          {
            clothName: "hat_001",
            id: 12
          },
          {
            clothName: "hat_001",
            id: 13
          },
          {
            clothName: "hat_001",
            id: 14
          },
          {
            clothName: "hat_001",
            id: 15
          },
          {
            clothName: "hat_001",
            id: 16
          },
          {
            clothName: "hat_001",
            id: 17
          },
          {
            clothName: "hat_002",
            id: 18
          },
          {
            clothName: "hat_002",
            id: 19
          },
          {
            clothName: "hat_002",
            id: 20
          },
          {
            clothName: "hat_002",
            id: 21
          },
          {
            clothName: "hat_002",
            id: 22
          },
          {
            clothName: "hat_002",
            id: 23
          },
          {
            clothName: "hat_002",
            id: 24
          },
          {
            clothName: "hat_002",
            id: 25
          },
          {
            clothName: "hat_003",
            id: 26
          },
          {
            clothName: "hat_003",
            id: 27
          },
          {
            clothName: "hat_003",
            id: 28
          },
          {
            clothName: "hat_003",
            id: 29
          },
          {
            clothName: "hat_003",
            id: 30
          },
          {
            clothName: "hat_003",
            id: 31
          },
          {
            clothName: "hat_003",
            id: 32
          },
          {
            clothName: "hat_003",
            id: 33
          },
          {
            clothName: "hat_004",
            id: 34
          },
          {
            clothName: "hat_004",
            id: 35
          },
          {
            clothName: "hat_004",
            id: 36
          },
          {
            clothName: "hat_004",
            id: 37
          },
          {
            clothName: "hat_004",
            id: 38
          },
          {
            clothName: "hat_004",
            id: 39
          },
          {
            clothName: "hat_004",
            id: 40
          },
          {
            clothName: "hat_004",
            id: 41
          },
          {
            clothName: "hat_005",
            id: 42
          },
          {
            clothName: "hat_005",
            id: 43
          },
          {
            clothName: "hat_005",
            id: 44
          },
          {
            clothName: "hat_005",
            id: 45
          },
          {
            clothName: "hat_005",
            id: 46
          },
          {
            clothName: "hat_005",
            id: 47
          },
          {
            clothName: "hat_005",
            id: 48
          },
          {
            clothName: "hat_005",
            id: 49
          },
          {
            clothName: "hat_006",
            id: 50
          },
          {
            clothName: "hat_006",
            id: 51
          },
          {
            clothName: "hat_006",
            id: 52
          },
          {
            clothName: "hat_006",
            id: 53
          },
          {
            clothName: "hat_006",
            id: 54
          },
          {
            clothName: "hat_006",
            id: 55
          },
          {
            clothName: "hat_006",
            id: 56
          },
          {
            clothName: "hat_006",
            id: 57
          },
          {
            clothName: "hat_007",
            id: 58
          },
          {
            clothName: "hat_007",
            id: 59
          },
          {
            clothName: "hat_007",
            id: 60
          },
          {
            clothName: "hat_007",
            id: 61
          },
          {
            clothName: "hat_007",
            id: 62
          },
          {
            clothName: "hat_007",
            id: 63
          },
          {
            clothName: "hat_007",
            id: 64
          },
          {
            clothName: "hat_007",
            id: 65
          },
          {
            clothName: "hat_008",
            id: 66
          },
          {
            clothName: "hat_008",
            id: 67
          },
          {
            clothName: "hat_008",
            id: 68
          },
          {
            clothName: "hat_008",
            id: 69
          },
          {
            clothName: "hat_008",
            id: 70
          },
          {
            clothName: "hat_008",
            id: 71
          },
          {
            clothName: "hat_008",
            id: 72
          },
          {
            clothName: "hat_008",
            id: 73
          },
          {
            clothName: "hat_009",
            id: 74
          },
          {
            clothName: "hat_009",
            id: 75
          },
          {
            clothName: "hat_009",
            id: 76
          },
          {
            clothName: "hat_009",
            id: 77
          },
          {
            clothName: "hat_009",
            id: 78
          },
          {
            clothName: "hat_009",
            id: 79
          },
          {
            clothName: "hat_009",
            id: 80
          },
          {
            clothName: "hat_009",
            id: 81
          },
          {
            clothName: "hat_010",
            id: 82
          },
          {
            clothName: "hat_010",
            id: 83
          },
          {
            clothName: "hat_010",
            id: 84
          },
          {
            clothName: "hat_010",
            id: 85
          },
          {
            clothName: "hat_010",
            id: 86
          },
          {
            clothName: "hat_010",
            id: 87
          },
          {
            clothName: "hat_010",
            id: 88
          },
          {
            clothName: "hat_010",
            id: 89
          },
          {
            clothName: "hat_011",
            id: 90
          },
          {
            clothName: "hat_011",
            id: 91
          },
          {
            clothName: "hat_011",
            id: 92
          },
          {
            clothName: "hat_011",
            id: 93
          },
          {
            clothName: "hat_011",
            id: 94
          },
          {
            clothName: "hat_011",
            id: 95
          },
          {
            clothName: "hat_011",
            id: 96
          },
          {
            clothName: "hat_011",
            id: 97
          },
          {
            clothName: "hat_012",
            id: 98
          },
          {
            clothName: "hat_012",
            id: 99
          },
          {
            clothName: "hat_012",
            id: 100
          },
          {
            clothName: "hat_012",
            id: 101
          },
          {
            clothName: "hat_012",
            id: 102
          },
          {
            clothName: "hat_012",
            id: 103
          },
          {
            clothName: "hat_012",
            id: 104
          },
          {
            clothName: "hat_012",
            id: 105
          },
          {
            clothName: "hat_013",
            id: 106
          },
          {
            clothName: "hat_013",
            id: 107
          },
          {
            clothName: "hat_013",
            id: 108
          },
          {
            clothName: "hat_013",
            id: 109
          },
          {
            clothName: "hat_013",
            id: 110
          },
          {
            clothName: "hat_013",
            id: 111
          },
          {
            clothName: "hat_013",
            id: 112
          },
          {
            clothName: "hat_013",
            id: 113
          },
          {
            clothName: "hat_014",
            id: 114
          },
          {
            clothName: "hat_014",
            id: 115
          },
          {
            clothName: "hat_014",
            id: 116
          },
          {
            clothName: "hat_014",
            id: 117
          },
          {
            clothName: "hat_014",
            id: 118
          },
          {
            clothName: "hat_014",
            id: 119
          },
          {
            clothName: "hat_014",
            id: 120
          },
          {
            clothName: "hat_014",
            id: 121
          },
          {
            clothName: "hat_015",
            id: 122
          },
          {
            clothName: "hat_015",
            id: 123
          },
          {
            clothName: "hat_015",
            id: 124
          },
          {
            clothName: "hat_015",
            id: 125
          },
          {
            clothName: "hat_015",
            id: 126
          },
          {
            clothName: "hat_015",
            id: 127
          },
          {
            clothName: "hat_015",
            id: 128
          },
          {
            clothName: "hat_015",
            id: 129
          },
          {
            clothName: "hat_016",
            id: 130
          },
          {
            clothName: "hat_016",
            id: 131
          },
          {
            clothName: "hat_016",
            id: 132
          },
          {
            clothName: "hat_016",
            id: 133
          },
          {
            clothName: "hat_016",
            id: 134
          },
          {
            clothName: "hat_016",
            id: 135
          },
          {
            clothName: "hat_016",
            id: 136
          },
          {
            clothName: "hat_016",
            id: 137
          },
          {
            clothName: "hat_017",
            id: 138
          },
          {
            clothName: "hat_017",
            id: 139
          },
          {
            clothName: "hat_017",
            id: 140
          },
          {
            clothName: "hat_017",
            id: 141
          },
          {
            clothName: "hat_017",
            id: 142
          },
          {
            clothName: "hat_017",
            id: 143
          },
          {
            clothName: "hat_017",
            id: 144
          },
          {
            clothName: "hat_017",
            id: 145
          },
          {
            clothName: "hat_018",
            id: 146
          },
          {
            clothName: "hat_018",
            id: 147
          },
          {
            clothName: "hat_018",
            id: 148
          },
          {
            clothName: "hat_018",
            id: 149
          },
          {
            clothName: "hat_018",
            id: 150
          },
          {
            clothName: "hat_018",
            id: 151
          },
          {
            clothName: "hat_018",
            id: 152
          },
          {
            clothName: "hat_018",
            id: 153
          },
          {
            clothName: "hat_019",
            id: 154
          },
          {
            clothName: "hat_019",
            id: 155
          },
          {
            clothName: "hat_019",
            id: 156
          },
          {
            clothName: "hat_019",
            id: 157
          },
          {
            clothName: "hat_019",
            id: 158
          },
          {
            clothName: "hat_019",
            id: 159
          },
          {
            clothName: "hat_019",
            id: 160
          },
          {
            clothName: "hat_019",
            id: 161
          },
          {
            clothName: "m_hat_034",
            id: 162
          },
          {
            clothName: "m_hat_034",
            id: 163
          },
          {
            clothName: "m_hat_034",
            id: 164
          },
          {
            clothName: "m_hat_034",
            id: 165
          },
          {
            clothName: "m_hat_034",
            id: 166
          },
          {
            clothName: "m_hat_034",
            id: 167
          },
          {
            clothName: "m_hat_034",
            id: 168
          },
          {
            clothName: "m_hat_034",
            id: 169
          },
          {
            clothName: "m_hat_035",
            id: 170
          },
          {
            clothName: "m_hat_035",
            id: 171
          },
          {
            clothName: "m_hat_035",
            id: 172
          },
          {
            clothName: "m_hat_035",
            id: 173
          },
          {
            clothName: "m_hat_035",
            id: 174
          },
          {
            clothName: "m_hat_035",
            id: 175
          },
          {
            clothName: "m_hat_035",
            id: 176
          },
          {
            clothName: "m_hat_035",
            id: 177
          },
          {
            clothName: "m_hat_036",
            id: 178
          },
          {
            clothName: "m_hat_036",
            id: 179
          },
          {
            clothName: "m_hat_036",
            id: 180
          },
          {
            clothName: "m_hat_036",
            id: 181
          },
          {
            clothName: "m_hat_036",
            id: 182
          },
          {
            clothName: "m_hat_036",
            id: 183
          },
          {
            clothName: "m_hat_036",
            id: 184
          },
          {
            clothName: "m_hat_036",
            id: 185
          },
          {
            clothName: "m_hat_037",
            id: 186
          },
          {
            clothName: "m_hat_037",
            id: 187
          },
          {
            clothName: "m_hat_037",
            id: 188
          },
          {
            clothName: "m_hat_037",
            id: 189
          },
          {
            clothName: "m_hat_037",
            id: 190
          },
          {
            clothName: "m_hat_037",
            id: 191
          },
          {
            clothName: "m_hat_037",
            id: 192
          },
          {
            clothName: "m_hat_037",
            id: 193
          },
          {
            clothName: "m_hat_038",
            id: 194
          },
          {
            clothName: "m_hat_038",
            id: 195
          },
          {
            clothName: "m_hat_038",
            id: 196
          },
          {
            clothName: "m_hat_038",
            id: 197
          },
          {
            clothName: "m_hat_038",
            id: 198
          },
          {
            clothName: "m_hat_038",
            id: 199
          },
          {
            clothName: "m_hat_038",
            id: 200
          },
          {
            clothName: "m_hat_038",
            id: 201
          },
          {
            clothName: "m_hat_039",
            id: 202
          },
          {
            clothName: "m_hat_039",
            id: 203
          },
          {
            clothName: "m_hat_039",
            id: 204
          },
          {
            clothName: "m_hat_039",
            id: 205
          },
          {
            clothName: "m_hat_039",
            id: 206
          },
          {
            clothName: "m_hat_039",
            id: 207
          },
          {
            clothName: "m_hat_039",
            id: 208
          },
          {
            clothName: "m_hat_039",
            id: 209
          },
          {
            clothName: "m_hat_040",
            id: 210
          },
          {
            clothName: "m_hat_040",
            id: 211
          },
          {
            clothName: "m_hat_040",
            id: 212
          },
          {
            clothName: "m_hat_040",
            id: 213
          },
          {
            clothName: "m_hat_040",
            id: 214
          },
          {
            clothName: "m_hat_040",
            id: 215
          },
          {
            clothName: "m_hat_040",
            id: 216
          },
          {
            clothName: "m_hat_040",
            id: 217
          },
          {
            clothName: "m_hat_041",
            id: 218
          },
          {
            clothName: "m_hat_041",
            id: 219
          },
          {
            clothName: "m_hat_041",
            id: 220
          },
          {
            clothName: "m_hat_041",
            id: 221
          },
          {
            clothName: "m_hat_041",
            id: 222
          },
          {
            clothName: "m_hat_041",
            id: 223
          },
          {
            clothName: "m_hat_041",
            id: 224
          },
          {
            clothName: "m_hat_041",
            id: 225
          },
          {
            clothName: "m_hat_042",
            id: 226
          },
          {
            clothName: "m_hat_042",
            id: 227
          },
          {
            clothName: "m_hat_042",
            id: 228
          },
          {
            clothName: "m_hat_042",
            id: 229
          },
          {
            clothName: "m_hat_042",
            id: 230
          },
          {
            clothName: "m_hat_042",
            id: 231
          },
          {
            clothName: "m_hat_042",
            id: 232
          },
          {
            clothName: "m_hat_042",
            id: 233
          },
          {
            clothName: "m_hat_043",
            id: 234
          },
          {
            clothName: "m_hat_043",
            id: 235
          },
          {
            clothName: "m_hat_043",
            id: 236
          },
          {
            clothName: "m_hat_043",
            id: 237
          },
          {
            clothName: "m_hat_043",
            id: 238
          },
          {
            clothName: "m_hat_043",
            id: 239
          },
          {
            clothName: "m_hat_043",
            id: 240
          },
          {
            clothName: "m_hat_043",
            id: 241
          },
          {
            clothName: "m_hat_044",
            id: 242
          },
          {
            clothName: "m_hat_044",
            id: 243
          },
          {
            clothName: "m_hat_044",
            id: 244
          },
          {
            clothName: "m_hat_044",
            id: 245
          },
          {
            clothName: "m_hat_044",
            id: 246
          },
          {
            clothName: "m_hat_044",
            id: 247
          },
          {
            clothName: "m_hat_044",
            id: 248
          },
          {
            clothName: "m_hat_044",
            id: 249
          },
          {
            clothName: "m_hat_045",
            id: 250
          },
          {
            clothName: "m_hat_045",
            id: 251
          },
          {
            clothName: "m_hat_045",
            id: 252
          },
          {
            clothName: "m_hat_045",
            id: 253
          },
          {
            clothName: "m_hat_045",
            id: 254
          },
          {
            clothName: "m_hat_045",
            id: 255
          },
          {
            clothName: "m_hat_045",
            id: 256
          },
          {
            clothName: "m_hat_045",
            id: 257
          },
          {
            clothName: "m_hat_046",
            id: 258
          },
          {
            clothName: "m_hat_046",
            id: 259
          },
          {
            clothName: "m_hat_046",
            id: 260
          },
          {
            clothName: "m_hat_046",
            id: 261
          },
          {
            clothName: "m_hat_046",
            id: 262
          },
          {
            clothName: "m_hat_046",
            id: 263
          },
          {
            clothName: "m_hat_046",
            id: 264
          },
          {
            clothName: "m_hat_046",
            id: 265
          },
          {
            clothName: "m_hat_047",
            id: 266
          },
          {
            clothName: "m_hat_047",
            id: 267
          },
          {
            clothName: "m_hat_047",
            id: 268
          },
          {
            clothName: "m_hat_047",
            id: 269
          },
          {
            clothName: "m_hat_047",
            id: 270
          },
          {
            clothName: "m_hat_047",
            id: 271
          },
          {
            clothName: "m_hat_047",
            id: 272
          },
          {
            clothName: "m_hat_047",
            id: 273
          },
          {
            clothName: "m_hat_048",
            id: 274
          },
          {
            clothName: "m_hat_048",
            id: 275
          },
          {
            clothName: "m_hat_048",
            id: 276
          },
          {
            clothName: "m_hat_048",
            id: 277
          },
          {
            clothName: "m_hat_048",
            id: 278
          },
          {
            clothName: "m_hat_048",
            id: 279
          },
          {
            clothName: "m_hat_048",
            id: 280
          },
          {
            clothName: "m_hat_048",
            id: 281
          },
          {
            clothName: "m_hat_049",
            id: 282
          },
          {
            clothName: "m_hat_049",
            id: 283
          },
          {
            clothName: "m_hat_049",
            id: 284
          },
          {
            clothName: "m_hat_049",
            id: 285
          },
          {
            clothName: "m_hat_049",
            id: 286
          },
          {
            clothName: "m_hat_049",
            id: 287
          },
          {
            clothName: "m_hat_049",
            id: 288
          },
          {
            clothName: "m_hat_049",
            id: 289
          },
          {
            clothName: "m_hat_050",
            id: 290
          },
          {
            clothName: "m_hat_050",
            id: 291
          },
          {
            clothName: "m_hat_050",
            id: 292
          },
          {
            clothName: "m_hat_050",
            id: 293
          },
          {
            clothName: "m_hat_050",
            id: 294
          },
          {
            clothName: "m_hat_050",
            id: 295
          },
          {
            clothName: "m_hat_050",
            id: 296
          },
          {
            clothName: "m_hat_050",
            id: 297
          },
          {
            clothName: "m_hat_051",
            id: 298
          },
          {
            clothName: "m_hat_051",
            id: 299
          },
          {
            clothName: "m_hat_051",
            id: 300
          },
          {
            clothName: "m_hat_051",
            id: 301
          },
          {
            clothName: "m_hat_051",
            id: 302
          },
          {
            clothName: "m_hat_051",
            id: 303
          },
          {
            clothName: "m_hat_051",
            id: 304
          },
          {
            clothName: "m_hat_051",
            id: 305
          },
          {
            clothName: "m_hat_052",
            id: 306
          },
          {
            clothName: "m_hat_052",
            id: 307
          },
          {
            clothName: "m_hat_052",
            id: 308
          },
          {
            clothName: "m_hat_052",
            id: 309
          },
          {
            clothName: "m_hat_052",
            id: 310
          },
          {
            clothName: "m_hat_052",
            id: 311
          },
          {
            clothName: "m_hat_052",
            id: 312
          },
          {
            clothName: "m_hat_052",
            id: 313
          },
          {
            clothName: "m_hat_053",
            id: 314
          },
          {
            clothName: "m_hat_053",
            id: 315
          },
          {
            clothName: "m_hat_053",
            id: 316
          },
          {
            clothName: "m_hat_053",
            id: 317
          },
          {
            clothName: "m_hat_053",
            id: 318
          },
          {
            clothName: "m_hat_053",
            id: 319
          },
          {
            clothName: "m_hat_053",
            id: 320
          },
          {
            clothName: "m_hat_053",
            id: 321
          },
          {
            clothName: "m_hat_054",
            id: 322
          },
          {
            clothName: "m_hat_054",
            id: 323
          },
          {
            clothName: "m_hat_054",
            id: 324
          },
          {
            clothName: "m_hat_054",
            id: 325
          },
          {
            clothName: "m_hat_054",
            id: 326
          },
          {
            clothName: "m_hat_054",
            id: 327
          },
          {
            clothName: "m_hat_054",
            id: 328
          },
          {
            clothName: "m_hat_054",
            id: 329
          },
          {
            clothName: "m_hat_055",
            id: 330
          },
          {
            clothName: "m_hat_055",
            id: 331
          },
          {
            clothName: "m_hat_055",
            id: 332
          },
          {
            clothName: "m_hat_055",
            id: 333
          },
          {
            clothName: "m_hat_055",
            id: 334
          },
          {
            clothName: "m_hat_055",
            id: 335
          },
          {
            clothName: "m_hat_055",
            id: 336
          },
          {
            clothName: "m_hat_055",
            id: 337
          },
          {
            clothName: "m_hat_056",
            id: 338
          },
          {
            clothName: "m_hat_056",
            id: 339
          },
          {
            clothName: "m_hat_056",
            id: 340
          },
          {
            clothName: "m_hat_056",
            id: 341
          },
          {
            clothName: "m_hat_056",
            id: 342
          },
          {
            clothName: "m_hat_056",
            id: 343
          },
          {
            clothName: "m_hat_056",
            id: 344
          },
          {
            clothName: "m_hat_056",
            id: 345
          },
          {
            clothName: "m_hat_057",
            id: 346
          },
          {
            clothName: "m_hat_057",
            id: 347
          },
          {
            clothName: "m_hat_057",
            id: 348
          },
          {
            clothName: "m_hat_057",
            id: 349
          },
          {
            clothName: "m_hat_057",
            id: 350
          },
          {
            clothName: "m_hat_057",
            id: 351
          },
          {
            clothName: "m_hat_057",
            id: 352
          },
          {
            clothName: "m_hat_057",
            id: 353
          },
          {
            clothName: "m_hat_058",
            id: 354
          },
          {
            clothName: "m_hat_058",
            id: 355
          },
          {
            clothName: "m_hat_058",
            id: 356
          },
          {
            clothName: "m_hat_058",
            id: 357
          },
          {
            clothName: "m_hat_058",
            id: 358
          },
          {
            clothName: "m_hat_058",
            id: 359
          },
          {
            clothName: "m_hat_058",
            id: 360
          },
          {
            clothName: "m_hat_058",
            id: 361
          },
          {
            clothName: "m_hat_200",
            id: 362
          },
          {
            clothName: "m_hat_200",
            id: 363
          },
          {
            clothName: "m_hat_200",
            id: 364
          },
          {
            clothName: "m_hat_200",
            id: 365
          },
          {
            clothName: "m_hat_200",
            id: 366
          },
          {
            clothName: "m_hat_200",
            id: 367
          },
          {
            clothName: "m_hat_200",
            id: 368
          },
          {
            clothName: "m_hat_200",
            id: 369
          },
          {
            clothName: "m_hat_201",
            id: 370
          },
          {
            clothName: "m_hat_201",
            id: 371
          },
          {
            clothName: "m_hat_201",
            id: 372
          },
          {
            clothName: "m_hat_201",
            id: 373
          },
          {
            clothName: "m_hat_201",
            id: 374
          },
          {
            clothName: "m_hat_201",
            id: 375
          },
          {
            clothName: "m_hat_201",
            id: 376
          },
          {
            clothName: "m_hat_201",
            id: 377
          },
          {
            clothName: "m_hat_202",
            id: 378
          },
          {
            clothName: "m_hat_202",
            id: 379
          },
          {
            clothName: "m_hat_202",
            id: 380
          },
          {
            clothName: "m_hat_202",
            id: 381
          },
          {
            clothName: "m_hat_202",
            id: 382
          },
          {
            clothName: "m_hat_202",
            id: 383
          },
          {
            clothName: "m_hat_202",
            id: 384
          },
          {
            clothName: "m_hat_202",
            id: 385
          },
          {
            clothName: "m_hat_203",
            id: 386
          },
          {
            clothName: "m_hat_203",
            id: 387
          },
          {
            clothName: "m_hat_203",
            id: 388
          },
          {
            clothName: "m_hat_203",
            id: 389
          },
          {
            clothName: "m_hat_203",
            id: 390
          },
          {
            clothName: "m_hat_203",
            id: 391
          },
          {
            clothName: "m_hat_203",
            id: 392
          },
          {
            clothName: "m_hat_203",
            id: 393
          },
          {
            clothName: "m_hat_204",
            id: 394
          },
          {
            clothName: "m_hat_204",
            id: 395
          },
          {
            clothName: "m_hat_204",
            id: 396
          },
          {
            clothName: "m_hat_204",
            id: 397
          },
          {
            clothName: "m_hat_204",
            id: 398
          },
          {
            clothName: "m_hat_204",
            id: 399
          },
          {
            clothName: "m_hat_204",
            id: 400
          },
          {
            clothName: "m_hat_204",
            id: 401
          },
          {
            clothName: "m_hat_205",
            id: 402
          },
          {
            clothName: "m_hat_205",
            id: 403
          },
          {
            clothName: "m_hat_205",
            id: 404
          },
          {
            clothName: "m_hat_205",
            id: 405
          },
          {
            clothName: "m_hat_205",
            id: 406
          },
          {
            clothName: "m_hat_205",
            id: 407
          },
          {
            clothName: "m_hat_205",
            id: 408
          },
          {
            clothName: "m_hat_205",
            id: 409
          },
          {
            clothName: "m_hat_206",
            id: 410
          },
          {
            clothName: "m_hat_206",
            id: 411
          },
          {
            clothName: "m_hat_206",
            id: 412
          },
          {
            clothName: "m_hat_206",
            id: 413
          },
          {
            clothName: "m_hat_206",
            id: 414
          },
          {
            clothName: "m_hat_206",
            id: 415
          },
          {
            clothName: "m_hat_206",
            id: 416
          },
          {
            clothName: "m_hat_206",
            id: 417
          },
          {
            clothName: "m_hat_207",
            id: 418
          },
          {
            clothName: "m_hat_207",
            id: 419
          },
          {
            clothName: "m_hat_207",
            id: 420
          },
          {
            clothName: "m_hat_207",
            id: 421
          },
          {
            clothName: "m_hat_207",
            id: 422
          },
          {
            clothName: "m_hat_207",
            id: 423
          },
          {
            clothName: "m_hat_207",
            id: 424
          },
          {
            clothName: "m_hat_207",
            id: 425
          },
          {
            clothName: "m_hat_208",
            id: 426
          },
          {
            clothName: "m_hat_208",
            id: 427
          },
          {
            clothName: "m_hat_208",
            id: 428
          },
          {
            clothName: "m_hat_208",
            id: 429
          },
          {
            clothName: "m_hat_208",
            id: 430
          },
          {
            clothName: "m_hat_208",
            id: 431
          },
          {
            clothName: "m_hat_208",
            id: 432
          },
          {
            clothName: "m_hat_208",
            id: 433
          },
          {
            clothName: "m_hat_209",
            id: 434
          },
          {
            clothName: "m_hat_209",
            id: 435
          },
          {
            clothName: "m_hat_209",
            id: 436
          },
          {
            clothName: "m_hat_209",
            id: 437
          },
          {
            clothName: "m_hat_209",
            id: 438
          },
          {
            clothName: "m_hat_209",
            id: 439
          },
          {
            clothName: "m_hat_209",
            id: 440
          },
          {
            clothName: "m_hat_209",
            id: 441
          },
          {
            clothName: "m_hat_210",
            id: 442
          },
          {
            clothName: "m_hat_210",
            id: 443
          },
          {
            clothName: "m_hat_210",
            id: 444
          },
          {
            clothName: "m_hat_210",
            id: 445
          },
          {
            clothName: "m_hat_210",
            id: 446
          },
          {
            clothName: "m_hat_210",
            id: 447
          },
          {
            clothName: "m_hat_210",
            id: 448
          },
          {
            clothName: "m_hat_210",
            id: 449
          },
          {
            clothName: "m_hat_211",
            id: 450
          },
          {
            clothName: "m_hat_211",
            id: 451
          },
          {
            clothName: "m_hat_211",
            id: 452
          },
          {
            clothName: "m_hat_211",
            id: 453
          },
          {
            clothName: "m_hat_211",
            id: 454
          },
          {
            clothName: "m_hat_211",
            id: 455
          },
          {
            clothName: "m_hat_211",
            id: 456
          },
          {
            clothName: "m_hat_211",
            id: 457
          },
          {
            clothName: "m_hat_212",
            id: 458
          },
          {
            clothName: "m_hat_212",
            id: 459
          },
          {
            clothName: "m_hat_212",
            id: 460
          },
          {
            clothName: "m_hat_212",
            id: 461
          },
          {
            clothName: "m_hat_212",
            id: 462
          },
          {
            clothName: "m_hat_212",
            id: 463
          },
          {
            clothName: "m_hat_212",
            id: 464
          },
          {
            clothName: "m_hat_212",
            id: 465
          },
          {
            clothName: "m_hat_213",
            id: 466
          },
          {
            clothName: "m_hat_213",
            id: 467
          },
          {
            clothName: "m_hat_213",
            id: 468
          },
          {
            clothName: "m_hat_213",
            id: 469
          },
          {
            clothName: "m_hat_213",
            id: 470
          },
          {
            clothName: "m_hat_213",
            id: 471
          },
          {
            clothName: "m_hat_213",
            id: 472
          },
          {
            clothName: "m_hat_213",
            id: 473
          },
          {
            clothName: "m_hat_213",
            id: 474
          },
          {
            clothName: "m_hat_213",
            id: 475
          },
          {
            clothName: "m_hat_214",
            id: 476
          },
          {
            clothName: "m_hat_214",
            id: 477
          },
          {
            clothName: "m_hat_214",
            id: 478
          },
          {
            clothName: "m_hat_214",
            id: 479
          },
          {
            clothName: "m_hat_214",
            id: 480
          },
          {
            clothName: "m_hat_214",
            id: 481
          },
          {
            clothName: "m_hat_214",
            id: 482
          },
          {
            clothName: "m_hat_214",
            id: 483
          },
          {
            clothName: "m_hat_215",
            id: 484
          },
          {
            clothName: "m_hat_215",
            id: 485
          },
          {
            clothName: "m_hat_215",
            id: 486
          },
          {
            clothName: "m_hat_215",
            id: 487
          },
          {
            clothName: "m_hat_215",
            id: 488
          },
          {
            clothName: "m_hat_215",
            id: 489
          },
          {
            clothName: "m_hat_215",
            id: 490
          },
          {
            clothName: "m_hat_215",
            id: 491
          },
          {
            clothName: "m_hat_216",
            id: 492
          },
          {
            clothName: "m_hat_216",
            id: 493
          },
          {
            clothName: "m_hat_216",
            id: 494
          },
          {
            clothName: "m_hat_216",
            id: 495
          },
          {
            clothName: "m_hat_216",
            id: 496
          },
          {
            clothName: "m_hat_216",
            id: 497
          },
          {
            clothName: "m_hat_216",
            id: 498
          },
          {
            clothName: "m_hat_216",
            id: 499
          },
          {
            clothName: "m_hat_217",
            id: 500
          },
          {
            clothName: "m_hat_217",
            id: 501
          },
          {
            clothName: "m_hat_217",
            id: 502
          },
          {
            clothName: "m_hat_217",
            id: 503
          },
          {
            clothName: "m_hat_217",
            id: 504
          },
          {
            clothName: "m_hat_217",
            id: 505
          },
          {
            clothName: "m_hat_217",
            id: 506
          },
          {
            clothName: "m_hat_217",
            id: 507
          },
          {
            clothName: "m_hat_218",
            id: 508
          },
          {
            clothName: "m_hat_218",
            id: 509
          },
          {
            clothName: "m_hat_218",
            id: 510
          },
          {
            clothName: "m_hat_218",
            id: 511
          },
          {
            clothName: "m_hat_218",
            id: 512
          },
          {
            clothName: "m_hat_218",
            id: 513
          },
          {
            clothName: "m_hat_218",
            id: 514
          },
          {
            clothName: "m_hat_218",
            id: 515
          },
          {
            clothName: "m_hat_219",
            id: 516
          },
          {
            clothName: "m_hat_219",
            id: 517
          },
          {
            clothName: "m_hat_219",
            id: 518
          },
          {
            clothName: "m_hat_219",
            id: 519
          },
          {
            clothName: "m_hat_219",
            id: 520
          },
          {
            clothName: "m_hat_219",
            id: 521
          },
          {
            clothName: "m_hat_219",
            id: 522
          },
          {
            clothName: "m_hat_219",
            id: 523
          },
          {
            clothName: "m_hat_220",
            id: 524
          },
          {
            clothName: "m_hat_220",
            id: 525
          },
          {
            clothName: "m_hat_220",
            id: 526
          },
          {
            clothName: "m_hat_220",
            id: 527
          },
          {
            clothName: "m_hat_220",
            id: 528
          },
          {
            clothName: "m_hat_220",
            id: 529
          },
          {
            clothName: "m_hat_220",
            id: 530
          },
          {
            clothName: "m_hat_220",
            id: 531
          },
          {
            clothName: "m_hat_221",
            id: 532
          },
          {
            clothName: "m_hat_221",
            id: 533
          },
          {
            clothName: "m_hat_221",
            id: 534
          },
          {
            clothName: "m_hat_221",
            id: 535
          },
          {
            clothName: "m_hat_221",
            id: 536
          },
          {
            clothName: "m_hat_221",
            id: 537
          },
          {
            clothName: "m_hat_221",
            id: 538
          },
          {
            clothName: "m_hat_221",
            id: 539
          },
          {
            clothName: "m_hat_222",
            id: 540
          },
          {
            clothName: "m_hat_222",
            id: 541
          },
          {
            clothName: "m_hat_222",
            id: 542
          },
          {
            clothName: "m_hat_222",
            id: 543
          },
          {
            clothName: "m_hat_222",
            id: 544
          },
          {
            clothName: "m_hat_222",
            id: 545
          },
          {
            clothName: "m_hat_222",
            id: 546
          },
          {
            clothName: "m_hat_222",
            id: 547
          },
          {
            clothName: "m_hat_223",
            id: 548
          },
          {
            clothName: "m_hat_223",
            id: 549
          },
          {
            clothName: "m_hat_223",
            id: 550
          },
          {
            clothName: "m_hat_223",
            id: 551
          },
          {
            clothName: "m_hat_223",
            id: 552
          },
          {
            clothName: "m_hat_223",
            id: 553
          },
          {
            clothName: "m_hat_223",
            id: 554
          },
          {
            clothName: "m_hat_223",
            id: 555
          },
          {
            clothName: "unknown_item",
            id: 556
          },
          {
            clothName: "unknown_item",
            id: 557
          },
          {
            clothName: "unknown_item",
            id: 558
          },
          {
            clothName: "unknown_item",
            id: 559
          },
          {
            clothName: "unknown_item",
            id: 560
          },
          {
            clothName: "unknown_item",
            id: 561
          },
          {
            clothName: "unknown_item",
            id: 562
          },
          {
            clothName: "unknown_item",
            id: 563
          },
          {
            clothName: "hat_000",
            id: 564
          },
          {
            clothName: "unknown_item",
            id: 565
          },
          {
            clothName: "hat_000",
            id: 566
          },
          {
            clothName: "hat_000",
            id: 567
          },
          {
            clothName: "hat_000",
            id: 568
          },
          {
            clothName: "unknown_item",
            id: 569
          },
          {
            clothName: "unknown_item",
            id: 570
          },
          {
            clothName: "hat_000",
            id: 571
          },
          {
            clothName: "hat_000",
            id: 572
          },
          {
            clothName: "hat_001",
            id: 573
          },
          {
            clothName: "unknown_item",
            id: 574
          }
        ],
        maxValue: 574,
        currentValue: 0,
        minValue: 0
      },
      {
        name: "cloaks",
        clothRef: [
          {
            clothName: "unknown_item",
            id: 1
          }
        ],
        maxValue: 1,
        currentValue: 0,
        minValue: 0
      }
    ],
    type: "allMenu"
  }

export default MockData