import React, { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHatCowboy, faTshirt, faSocks, faGlasses, faTools, faRing, faStar, faEye, faHeadphones, faGear } from '@fortawesome/free-solid-svg-icons'
import Face from '@material-design-icons/svg/filled/face.svg';
import Eye from '@material-design-icons/svg/filled/visibility.svg';
import Body from '@material-design-icons/svg/filled/accessibility.svg';
import Gear from '@material-design-icons/svg/filled/settings.svg';
import Ear from '@material-design-icons/svg/filled/hearing.svg';
import Mouth from '@material-design-icons/svg/filled/emoji_emotions.svg';
import Star from '@material-design-icons/svg/filled/star.svg';

import Constants from "./Constants.jsx";

library.add(faHatCowboy, faTshirt, faSocks, faGlasses, faTools, faRing, faStar, faEye, faHeadphones, )

const Clothing = Constants.Clothing
const Skin = Constants.Skin

export default function Sidebar({ navSelect, selectedSubMenu, onSubmenuClick }) {
  return (
    <div>
      {navSelect === 2 &&
        <div className='bookmark-container'>
          <button className={selectedSubMenu === Skin.General || selectedSubMenu === Skin.Default ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.General)} data-tip='Cabeza'>
            <Body style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.Eyes ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.Eyes)} data-tip='Cara'>
            <Eye style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.Nose ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.Nose)} data-tip='Cara'>
            <Face style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.Ears ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.Ears)} data-tip='Cara'>
            <Ear style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.Mouth ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.Mouth)} data-tip='Cara'>
            <Mouth style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.Extras ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.Extras)} data-tip='Cara'>
            <Gear style={{fill: "white"}}/>
          </button>
          <button className={selectedSubMenu === Skin.All ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Skin.All)} data-tip='Cara'>
            <Star style={{fill: "white"}}/>
          </button>
        </div>
      }
      {navSelect === 3 &&
        <div className='bookmark-container'>
          <button className={selectedSubMenu === Clothing.Head || selectedSubMenu === Clothing.Default ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.Head)} data-tip='Cabeza'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="hat-cowboy"/>
          </button>
          <button className={selectedSubMenu === Clothing.Chest ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.Chest)} data-tip='Torso'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="tshirt"/>
          </button>
          <button className={selectedSubMenu === Clothing.Legs ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.Legs)} data-tip='Piernas y pies'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="socks"/>
          </button>
          <button className={selectedSubMenu === Clothing.Tools ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.Tools)} data-tip='Cinturon y herramientas'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="tools"/>
          </button>
          <button className={selectedSubMenu === Clothing.Misc ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.Misc)} data-tip='Joyeria'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="ring"/>
          </button>
          <button className={selectedSubMenu === Clothing.All ? 'button button-bookmark active' : 'button button-bookmark'} onClick={() => onSubmenuClick(Clothing.All)} data-tip='Todo'>
            <FontAwesomeIcon className="FontAwesomeIcon" icon="star"/>
          </button>
        </div>
      }
    </div>
  );
}

