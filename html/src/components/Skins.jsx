import React, {useEffect, useState} from 'react'
import SkinItem from './SkinItem.jsx'
import Constants  from './Constants.jsx'

export default function Skins({selectedSubMenu, data}) {
  const handleSkinChange = (e) => {
    data.find((o, i) => {
      if(o.name === e.name) {
        data[i].currentValue = e.currentValue
        return
      }
    })

    fetch('https://qbr-clothing/applySkin', {method: 'POST', body: JSON.stringify({
      values: data
    })})
  }

  const filterBySubmenu = () => {
    switch (selectedSubMenu) {
      case Constants.Skin.General:
        return data.filter(function (item) {
          return Constants.GeneralSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.Eyes:
        return data.filter(function (item) {
          return Constants.EyesSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.Nose:
        return data.filter(function (item) {
          return Constants.NoseSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.Ears:
        return data.filter(function (item) {
          return Constants.EarsSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.Mouth:
        return data.filter(function (item) {
          return Constants.MouthSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.Extras:
        return data.filter(function (item) {
          return Constants.ExtrasSkin.indexOf(item['name']) !== -1;
        });
      case Constants.Skin.All:
        return data;
      default:
        return data.filter(function (item) {
          return Constants.GeneralSkin.indexOf(item['name']) !== -1;
        });;
    }
  }

  return (
    <div className="clothingMenu">
      <div className="clothingMenu_options">
        {filterBySubmenu().map((i, k) => <SkinItem data={i} key={k} onSkinChange={handleSkinChange} />)}
      </div>
    </div>
  )
}