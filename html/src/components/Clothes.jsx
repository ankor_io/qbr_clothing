import React, { useEffect, useState } from 'react'
import ClothItem from './ClothItem.jsx'
import Constants from './Constants.jsx'

export default function Clothes({ selectedSubMenu, data }) {
  const handleSkinChange = (e) => {
    data.find((o, i) => {
      if (o.name === e.name) {
        data[i].currentValue = e.currentValue
        return
      }
    })

    fetch('https://qbr-clothing/applyClothes', {
      method: 'POST', body: JSON.stringify({
        values: data
      })
    })
  }

  const filterBySubmenu = () => {
    switch (selectedSubMenu) {
      case Constants.Clothing.Head:
        return data.filter(function (item) {
          return Constants.HeadClothing.indexOf(item['name']) !== -1;
        });
      case Constants.Clothing.Chest:
        return data.filter(function (item) {
          return Constants.ChestClothing.indexOf(item['name']) !== -1;
        });
      case Constants.Clothing.Legs:
        return data.filter(function (item) {
          return Constants.LegsClothing.indexOf(item['name']) !== -1;
        });
      case Constants.Clothing.Misc:
        return data.filter(function (item) {
          return Constants.MiscClothing.indexOf(item['name']) !== -1;
        });
      case Constants.Clothing.Tools:
        return data.filter(function (item) {
          return Constants.ToolsClothing.indexOf(item['name']) !== -1;
        });
      case Constants.Clothing.All:
        return data;
      default:
        return data.filter(function (item) {
          return Constants.HeadClothing.indexOf(item['name']) !== -1;
        });
    }
  }

  return (
    <div className="clothingMenu">
      <div className="clothingMenu_options">
        {filterBySubmenu().map((item, index) => <ClothItem data={item}/>)}
      </div>
    </div>
  )
}