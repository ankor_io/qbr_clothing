const Clothing = {
    Head: 'clothhing-head',
    Chest: 'clothing-chest',
    Legs: 'clothing-legs',
    Tools: 'clothing-tools',
    Misc: 'clothing-misc',
    All: 'clothing-all',
    Default: 'none'
}
const HeadClothing = [
    'hats', 'eyewear'
]
const ChestClothing = [
    'shirts_full', 'coats', 'coats_closed', 'suspenders', 'vests'
]
const LegsClothing = [
    'skirts', 'boot_accessories', 'chaps', 'pants', 'boots', 'spats',
]
const ToolsClothing = [
    'loadouts', 'gunbelts', 'holsters_left', 'satchels'
]
const MiscClothing = [
    'neckwear', 'neckties', 'gauntlets', 'gloves', 'belt_buckles', 'jewelry_rings_left', 'ponchos', 'cloaks'
]

const Skin = {
    General: 'skin-head',
    Eyes: 'skin-face',
    Nose: 'skin-nose',
    Ears: 'skin-ears',
    Mouth: 'skin-mouth',
    Extras: 'skin-extras',
    All: 'skin-all',
    Default: 'none'
}
const GeneralSkin = [
    'beard', 'hair', 'heads', 'skin', 'face_width'
]
const EyesSkin = [
    'eyes', 'eyes_angle', 'eyebrow_height', 'eyelid_height', 'eyebrow_width',
    'eyelid_width', 'eyebrow_depth', 'eyes_heigth', 'eyes_distance', 'eyes_depth'
]
const NoseSkin = [
    'nose_width', 'nose_size', 'cheekbones_height', 'cheekbones_width', 
    'cheekbones_depth', 'nose_curvature', 'nose_angle', 'nose_height',
    'nostril_distance'
]
const EarsSkin = [
    'ears_size', 'ears_angle', 'ears_width', 'ears_height'
]
const MouthSkin = [
    'teeth', 'jaw_height', 'haw_width','chin_width', 'chin_depth',
    'mouth_depth', 'lower_lip_height', 'upper_lip_depth', 'lower_lip_depth',
    'mouth_x_pos', 'lower_lip_width', 'upper_lip_width', 'upper_lip_height', 
    'mouth_width', 'mouth_y_pos', 'chin_height', 'jaw_depth'
]
const ExtrasSkin = [

]

//General (Pelo, Barba, Bigote, Piel, Cara)
//Ojos (Ojos, extra ojos + cejas) 
//Nariz (Nariz)
//Orejas (Orejas)
//Boca (Boca + pomulos + mandibula)
//Extras (Cicatrizes + pecas + etc)


const Constants = {
    Clothing,
    // Constants used to filter based on submenu cloth_hash_names -> category_hashname
    HeadClothing,
    ChestClothing,
    LegsClothing,
    ToolsClothing,
    MiscClothing,
    Skin,
    GeneralSkin,
    EyesSkin,
    NoseSkin,
    EarsSkin,
    MouthSkin,
    ExtrasSkin
};
export default Constants
